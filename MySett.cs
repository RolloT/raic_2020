﻿using Aicup2020.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aicup2020
{
	public static class MySett
	{
		public static EntityType[] RangedAndMeleeAttackerTypes = new[] { EntityType.MeleeUnit, EntityType.RangedUnit };
		public static EntityType[] BuildingTypes = new[] { EntityType.BuilderBase, EntityType.RangedBase, EntityType.MeleeBase };
		public static EntityType[] AttackerTypes = new[] { EntityType.MeleeUnit, EntityType.RangedUnit, EntityType.Turret };
		public static EntityType[] NotToRepareTypes = new[] { EntityType.MeleeUnit, EntityType.RangedUnit, EntityType.BuilderUnit, EntityType.MeleeBase };
		public static UnitGroup SaboteursX = new UnitGroup { GroupType = UnitGroupTypes.SaboteurX };
		public static UnitGroup SaboteursY = new UnitGroup { GroupType = UnitGroupTypes.SaboteurY };


		public static EntityType[] EntitiesToLeaveSpaceBetween = new[] { EntityType.Resource, EntityType.BuilderBase, EntityType.House, EntityType.MeleeBase, EntityType.RangedBase, EntityType.Turret };

		public static bool IsFinal = false;
		public static bool IsR1 = false;
		public static bool IsR2 = false;

		public static bool? IsEnemyOnRight = null;
		public static bool IsLeftConnerClear = false;
		public static bool IsRightConnerClear = false;
		public static bool IsFarConnerClear = false;

		public static Vec2Int GetPointToGoIfNoTarget(bool localIsEnemyOnRight) {
			var myAttackersCount = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes).Count;
			var myAttackersMin = 6;

			var isEnemyOnRight = IsEnemyOnRight ?? localIsEnemyOnRight;
			if (isEnemyOnRight && !IsRightConnerClear)
			{
				if (myAttackersCount > myAttackersMin)
					return new Vec2Int (PlayerView.MapSize - 8, 8);
				else
					return new Vec2Int ((PlayerView.MapSize / 3), PlayerView.MapSize / 6 );
			}
			else if (!isEnemyOnRight && !IsLeftConnerClear) 
			{
				if (myAttackersCount > myAttackersMin)
					return new Vec2Int(8, PlayerView.MapSize - 8);
				else
					return new Vec2Int((PlayerView.MapSize / 6), PlayerView.MapSize / 3);
			}
			else if (!IsFarConnerClear)
			{
				if (myAttackersCount > myAttackersMin)
					return new Vec2Int(PlayerView.MapSize - 8, PlayerView.MapSize - 8);
				else
					return new Vec2Int((PlayerView.MapSize / 4), PlayerView.MapSize / 4);
			}
			else
			{
				return new Vec2Int((PlayerView.MapSize / 2), PlayerView.MapSize / 2);
			}
		}


		public static void InitMySett(ref PlayerView playerView) 
		{
			PlayerView = playerView;
			OneTimeInit();

			MyPlayer = playerView.Players.First(p => p.Id == MyPlayerId);
			MyEntityCategory = InitEntityCategoryDic();
			EnemyCategory = InitEntityCategoryDic();
			AllResources = new List<Entity>();
			MyAllEntity = new List<Entity>();
			MyEntityWithActionIds = new HashSet<int>();


			Map = new Map(ref playerView);


		}

		public static List<Entity> GetMyEntities(IEnumerable<EntityType> entTypes, bool exceptOnWork = false)
		{
			return entTypes.SelectMany(t => GetMyEntities(t, exceptOnWork)).ToList();
		}

		public static List<Entity> GetMyEntities(EntityType entType, bool exceptOnWork = false)
		{
			if (exceptOnWork)
			{
				return MyEntityCategory[entType].Where(e => !MyEntityWithActionIds.Contains(e.Id)).ToList();
			}

			return MyEntityCategory[entType];
		}

		public static List<Entity> GetEnemyEntities(IEnumerable<EntityType> entTypes)
		{
			return entTypes.SelectMany(t => GetEnemyEntities(t)).ToList();
		}

		public static List<Entity> GetEnemyEntities(EntityType entType)
		{
			return EnemyCategory[entType];
		}

		public static bool _isInit = false;
		public static void OneTimeInit() 
		{
			if (_isInit) return;
			MyPlayerId = PlayerView.MyId;
			IsFinal = PlayerView.Players.Length == 2;
			IsR1 = !PlayerView.FogOfWar;
			IsR2 = PlayerView.Players.Length == 4 && PlayerView.FogOfWar;
			_isInit = true;
		}

		public static Dictionary<EntityType, List<Entity>> InitEntityCategoryDic() 
		{
			var res = new Dictionary<EntityType, List<Entity>>();
			foreach(var t in Enum.GetValues<EntityType>())
			{
				res.Add(t, new List<Entity>());
			}

			return res;
		}

		public static int MyPlayerId;
		public static Dictionary<EntityType, List<Entity>> MyEntityCategory;
		public static Dictionary<EntityType, List<Entity>> EnemyCategory;
		public static Map Map;
		public static Player MyPlayer;
		public static List<Entity> AllResources;
		public static List<Entity> MyAllEntity;
		public static HashSet<int> MyEntityWithActionIds;
		public static PlayerView PlayerView;
	}
}
