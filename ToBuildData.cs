﻿using Aicup2020.Model;

namespace aicup2020
{
	public class ToBuildData
	{
		public Entity Builder { get; set; }
		public Vec2Int BuildPoint { get; set; }
	}
}
