﻿using Aicup2020.Model;
using System.Collections.Generic;
using System.Linq;

namespace aicup2020
{
	public class Map
	{
		public void SetEntityToMySett(ref Entity ent)
		{
			if (ent.EntityType == EntityType.Resource)
			{
				MySett.AllResources.Add(ent);
			}
			else if (ent.PlayerId == MySett.MyPlayerId)
			{
				MySett.MyEntityCategory[ent.EntityType].Add(ent);
				MySett.MyAllEntity.Add(ent);
			}
			else
			{
				MySett.EnemyCategory[ent.EntityType].Add(ent);
			}

			

		}

		public int ScanDist { get; set; }
		//public Dictionary<EntityType, List<Entity>> MyEntityCategory;

		private List<Entity> _emptyEntityList = new List<Entity>();
		/*
		public List<Entity> GetMyEntity(EntityType type)
		{
			if (!MyEntityCategory.ContainsKey(type)) return _emptyEntityList;

			return MyEntityCategory[type];
		}
		*/
		public List<MapBlock> MapBlocks10 = new List<MapBlock>();
		public List<MapBlock> MapBlocks20 = new List<MapBlock>();
		public List<MapBlock> MapBlocks40 = new List<MapBlock>();

		public void FillMapBlocks() 
		{ 
			for(var x = 0; x < PlayerView.MapSize; x++)
			{
				for (var y = 0; y < PlayerView.MapSize; y++)
				{
					var mapData = Items[x, y];
					var block = GetMapBlock(x, y, 10);
					FillMapBlock(block, ref mapData);
					block = GetMapBlock(x, y, 20);
					FillMapBlock(block, ref mapData);
					block = GetMapBlock(x, y, 40);
					FillMapBlock(block, ref mapData);
				}
			}
		}

		public void FillMapBlock(MapBlock block, ref MapItem mapData) 
		{
			block.AllMapItems.Add(mapData);
			block.SumData.AttackCroudIndex += mapData.AttackCroudIndex;
			block.SumData.AttackDamage += mapData.AttackDamage;
			block.SumData.AttackHealth += mapData.AttackHealth;
			block.SumData.AttackNear += mapData.AttackNear;
			block.SumData.AttackPower += mapData.AttackPower;

			block.SumData.MyCroudIndex += mapData.MyCroudIndex;
			block.SumData.MyAttackDamage += mapData.MyAttackDamage;
			block.SumData.MyAttackHealth += mapData.MyAttackHealth;
			block.SumData.MyAttackNear += mapData.MyAttackNear;
			block.SumData.MyAttackPower += mapData.MyAttackPower;
		}

		public void CreateMapBlocks(List<MapBlock> mapBlocks, int size)
		{
			if (size != 10) 
			{
				var num = 0;
				for (var lvl = 0; lvl < PlayerView.MapSize / size; lvl++)
				{
					for (var i = 0; i <= lvl; i++)
					{
						//По Y
						var block = new MapBlock
						{
							Lvl = lvl,
							Num = num,
							Position = new Vec2Int(lvl * size, i * size),
							Size = size,
						};
						mapBlocks.Add(block);

						//По X
						if (i != lvl)
						{
							block = new MapBlock
							{
								Lvl = lvl,
								Num = num,
								Position = new Vec2Int(i * size, lvl * size),
								Size = size,
							};
							mapBlocks.Add(block);
						}
						num++;
					}
				}

				return;
			}

			if (size == 10)
			{
				//MapBlock(int num, int lvl, int x, int y, int size) 
				mapBlocks.Add(new MapBlock(0, 0, 0, 0, 10));
				mapBlocks.Add(new MapBlock(1, 1, 0, 1, 10));
				mapBlocks.Add(new MapBlock(2, 1, 1, 0, 10));
				mapBlocks.Add(new MapBlock(3, 1, 1, 1, 10));
				mapBlocks.Add(new MapBlock(4, 2, 2, 0, 10));
				mapBlocks.Add(new MapBlock(5, 2, 2, 1, 10));
				mapBlocks.Add(new MapBlock(6, 2, 2, 2, 10));
				mapBlocks.Add(new MapBlock(7, 2, 0, 2, 10));
				mapBlocks.Add(new MapBlock(8, 2, 1, 2, 10));
				mapBlocks.Add(new MapBlock(9, 3, 3, 0, 10));
				mapBlocks.Add(new MapBlock(10, 3, 3, 1, 10));
				mapBlocks.Add(new MapBlock(11, 3, 3, 2, 10));
				mapBlocks.Add(new MapBlock(13, 3, 0, 3, 10));
				mapBlocks.Add(new MapBlock(14, 3, 1, 3, 10));
				mapBlocks.Add(new MapBlock(15, 3, 2, 3, 10));
				mapBlocks.Add(new MapBlock(16, 4, 4, 0, 10));
				mapBlocks.Add(new MapBlock(17, 4, 4, 1, 10));
				mapBlocks.Add(new MapBlock(18, 4, 4, 2, 10));
				mapBlocks.Add(new MapBlock(19, 4, 0, 4, 10));
				mapBlocks.Add(new MapBlock(20, 4, 1, 4, 10));
				mapBlocks.Add(new MapBlock(21, 4, 2, 4, 10));
				mapBlocks.Add(new MapBlock(22, 4, 3, 3, 10));
				mapBlocks.Add(new MapBlock(23, 5, 5, 0, 10));
				mapBlocks.Add(new MapBlock(24, 5, 5, 1, 10));
				mapBlocks.Add(new MapBlock(25, 5, 5, 2, 10));
				mapBlocks.Add(new MapBlock(26, 5, 0, 5, 10));
				mapBlocks.Add(new MapBlock(27, 5, 1, 5, 10));
				mapBlocks.Add(new MapBlock(28, 5, 2, 5, 10));
				mapBlocks.Add(new MapBlock(29, 5, 4, 3, 10));
				mapBlocks.Add(new MapBlock(29, 5, 3, 4, 10));
				mapBlocks.Add(new MapBlock(30, 6, 6, 0, 10));
				mapBlocks.Add(new MapBlock(31, 6, 6, 1, 10));
				mapBlocks.Add(new MapBlock(32, 6, 6, 2, 10));
				mapBlocks.Add(new MapBlock(33, 6, 0, 6, 10));
				mapBlocks.Add(new MapBlock(34, 6, 1, 6, 10));
				mapBlocks.Add(new MapBlock(35, 6, 2, 6, 10));
				mapBlocks.Add(new MapBlock(36, 6, 5, 3, 10));
				mapBlocks.Add(new MapBlock(37, 6, 4, 4, 10));
				mapBlocks.Add(new MapBlock(38, 6, 3, 5, 10));
				mapBlocks.Add(new MapBlock(39, 7, 7, 0, 10));
				mapBlocks.Add(new MapBlock(40, 7, 7, 1, 10));
				mapBlocks.Add(new MapBlock(41, 7, 7, 2, 10));
				mapBlocks.Add(new MapBlock(42, 7, 0, 7, 10));
				mapBlocks.Add(new MapBlock(43, 7, 1, 7, 10));
				mapBlocks.Add(new MapBlock(44, 7, 2, 7, 10));
				mapBlocks.Add(new MapBlock(45, 7, 5, 4, 10));
				mapBlocks.Add(new MapBlock(46, 7, 4, 5, 10));
				mapBlocks.Add(new MapBlock(47, 7, 6, 3, 10));
				mapBlocks.Add(new MapBlock(48, 7, 3, 6, 10));
				mapBlocks.Add(new MapBlock(49, 8, 7, 3, 10));
				mapBlocks.Add(new MapBlock(50, 8, 3, 7, 10));
				mapBlocks.Add(new MapBlock(51, 8, 6, 4, 10));
				mapBlocks.Add(new MapBlock(52, 8, 4, 6, 10));
				mapBlocks.Add(new MapBlock(53, 8, 5, 5, 10));
				mapBlocks.Add(new MapBlock(54, 9, 7, 4, 10));
				mapBlocks.Add(new MapBlock(55, 9, 4, 7, 10));
				mapBlocks.Add(new MapBlock(56, 9, 5, 6, 10));
				mapBlocks.Add(new MapBlock(57, 9, 6, 5, 10));
				mapBlocks.Add(new MapBlock(58, 10, 7, 5, 10));
				mapBlocks.Add(new MapBlock(59, 10, 5, 7, 10));
				mapBlocks.Add(new MapBlock(60, 10, 6, 6, 10));
				mapBlocks.Add(new MapBlock(61, 11, 6, 7, 10));
				mapBlocks.Add(new MapBlock(62, 11, 7, 6, 10));
				mapBlocks.Add(new MapBlock(63, 12, 7, 7, 10));
			}
		}

		public MapBlock GetMapBlock(int x, int y, int size)
		{
			List<MapBlock> list;
			switch(size)
			{
				case 10:
					list = MapBlocks10;
					break;
				case 20:
					list = MapBlocks20;
					break;
				case 40:
					list = MapBlocks40;
					break;
				default:
					return null;
			}

			return list.First(b => b.Position.X <= x && b.Position.Y <= y
				&& b.Position.X + size > x && b.Position.Y + size > y
			);
		}

		public Map(ref PlayerView playerView)
		{
			ScanDist = 9;
			//MyEntityCategory = myEntityCategory;
			Items = new MapItem[playerView.MapSize, playerView.MapSize];
			for (var x = 0; x < playerView.MapSize; x++)
			{
				for (var y = 0; y < playerView.MapSize; y++)
				{
					Items[x, y] = new MapItem();
				}
			}
			PlayerView = playerView;
			
			CreateMapBlocks(MapBlocks10, 10);
			CreateMapBlocks(MapBlocks20, 20);
			CreateMapBlocks(MapBlocks40, 40);

			Fill();
			FillMapBlocks();
		}
		public MapItem[,] Items { get; set; }
		public PlayerView PlayerView { get; set; }

		public void Fill()
		{
			var allEnemy = new List<int>();
			var allMy = new List<int>();
			for (var i = 0; i < PlayerView.Entities.Length; i++)
			{
				var ent = PlayerView.Entities[i];
				SetEntityToMySett(ref ent);
				if (ent.PlayerId == MySett.MyPlayerId) {
					allMy.Add(i);
				}
				else
				{
					allEnemy.Add(i);
				}
			}
			

			//РАсставляем всех
			for (var i = 0; i < allEnemy.Count; i++)
			{
				SetEntityPlace(ref PlayerView.Entities[allEnemy[i]]);
			}
			for (var i = 0; i < allMy.Count; i++)
			{
				SetEntityPlace(ref PlayerView.Entities[allMy[i]]);
			}

			//Считаем Атаку
			for (var i = 0; i < allEnemy.Count; i++)
			{
				SetEntityAttack(ref PlayerView.Entities[allEnemy[i]]);
			}
			for (var i = 0; i < allMy.Count; i++)
			{
				SetEntityAttack(ref PlayerView.Entities[allMy[i]]);
			}
		}

		public int GetDamage(int x, int y, bool near) {
			if (!IsValid(x, y)) return 100000;
			return Items[x, y].AttackDamage + (near ? Items[x, y].AttackNear : 0);
		}

		/// <summary>
		/// Если > 0, то мы побеждаем. Если < 0, то надо убегать. Если 0, то мы уже под атакой
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public double GetMyEntityAttackPowerBalance(int x, int y)
		{
			if (!IsValid(x, y)) return 0;
			var damage =  Items[x, y].AttackDamage;
			if (damage > 0) return 0;
			var myAttackPower = Items[x, y].AttackPower;

			//если хоть у одного атакующего меня AttackPowerBalance > MyAttackPowerBalance , то баланс отрицательный
			var allAttackers = Items[x, y].AttackedBy;
			var allAttackersNear = Items[x, y].AttackedByNear;

			if (allAttackersNear.Count == 0 || allAttackers.Count > 0)
				return 0;

			//Выключаем защиту для R1. оставляем только для турели 
			if (
				false //!MySett.PlayerView.FogOfWar    
				&& !allAttackersNear.Any(a => a.EntityType == EntityType.Turret)
				)
			{
				return 0;
			}


			/*
			if (allAttackers.Any(a => Items[a.Position.X, a.Position.Y].MyAttackPowerBalance < myBalance)) 
				return -1;
			*/

			//if (allAttackersNear.Any(a => Items[a.Position.X, a.Position.Y].MyAttackPower < myAttackPower * 0.3))
			//Проверяем баланс по самому ближнему ко мне
			var firstAttacker = allAttackersNear.OrderBy(a => MyUtils.Dist(a.Position.X, a.Position.Y, x, y)).First();
			if (Items[firstAttacker.Position.X, firstAttacker.Position.Y].MyAttackPower < myAttackPower * 1.0) 
			{
				return -1;
			}

			return 1;
		}


		public int CountMyFriendsArround(Entity entity, int radius) {
			return MySett.MyEntityCategory[EntityType.RangedUnit].Count(e => e.Id != entity.Id && MyUtils.Dist(entity.Position, e.Position) < radius) 
				+ MySett.MyEntityCategory[EntityType.MeleeUnit].Count(e => e.Id != entity.Id && MyUtils.Dist(entity.Position, e.Position) < radius);
		}

		/// <summary>
		/// Позиция для моего атакующего, чтобы столпиться
		/// </summary>
		/// <param name="point"></param>
		/// <returns></returns>
		public Vec2Int? GetMyAttackerPointToCroud(Entity entity)
		{
			var point = entity.Position;
			//TODO: Пересчитывать позиции на карте по мере расчетного перемещения юнитов
			if (!IsValid(point.X, point.Y)) return null;
			var currData = Items[point.X, point.Y];

			//
			//if (currData.AttackDamage > 0) 
			if (currData.AttackCroudIndex == 0 || currData.AttackDamage > 0)
				return null;

			var myFriends = CountMyFriendsArround(entity, 11);
			if (myFriends < 1) return null;
			myFriends = System.Math.Min(myFriends, 2);
			var needIndex = 10;
			var baseInd = (ScanDist);
			switch (myFriends) {
				case 1:
					needIndex = baseInd * 2 - 1;
					break;
				case 2:
					needIndex = baseInd * 2 - 1 + baseInd-2;
					break;
				case 3:
					needIndex = baseInd*2 -1 + baseInd-2 + baseInd-3;
					break;
				case 4:
					needIndex = baseInd * 2 - 1 + baseInd - 2 + baseInd - 3 + baseInd - 4;
					break;
			}
			if (currData.MyCroudIndex > needIndex) 
				return null;

			var currCroudIndex = currData.MyCroudIndex - ScanDist;
			var maxPoint = point;
			var findBetter = false;

			for (var x = point.X - 1; x <= point.X + 1; x++)
			{
				for (var y = point.Y - 1; y <= point.Y + 1; y++)
				{
					if (!IsValid(x, y)) continue;
					if (x != point.X && y != point.Y) continue;

					var data = Items[x, y];
					if (data.Entity != null) continue;

					if (currCroudIndex >= data.MyCroudIndex - ScanDist + 1) continue;

					maxPoint = new Vec2Int(x, y);
					findBetter = true;
					currCroudIndex = data.MyCroudIndex - ScanDist + 1;
					continue;
				}
			}

			return findBetter ? maxPoint : null;
		}

		/// <summary>
		/// Позиция для моего атакующего, чтобы не попасть в замес 
		/// </summary>
		/// <param name="point"></param>
		/// <returns></returns>
		public Vec2Int? GetMyAttackerPointToSaveFromAttack(Vec2Int point)
		{
			//TODO: Пересчитывать позиции на карте по мере расчетного перемещения юнитов
			if (!IsValid(point.X, point.Y)) return null;
			var itemData = Items[point.X, point.Y];
			var currAttackPower = itemData.AttackPower;

			//Если нас атакуют. null //TODO: Если лучник и некуда отступать из зоны атаки. Автоатаку все равно оставляем 5
			if (itemData.AttackDamage > 0) return null;

			//Если один из атакующих турель и мы до нее достаем, то стоим на месте
			
			if (itemData.AttackedByNear.Count == 1 && itemData.AttackedByNear.Any(e=> e.EntityType == EntityType.Turret))
			{
				return point;
			}
			
			//TODO: Если рядом есть друзья, то стоим на месте


			var maxPoint = point;
			var findBetter = false;

			for (var x = point.X - 1; x <= point.X + 1; x++)
			{
				for (var y = point.Y - 1; y <= point.Y + 1; y++)
				{
					if (!IsValid(x, y)) continue;
					//По диагонали не ходим
					if (x != point.X && y != point.Y) continue;

					//Не отступаем за середину поля
					if (MySett.PlayerView.CurrentTick < 350) 
					{ 
						if (point.X > 40 && point.X < 45 && x < point.X) continue;
						if (point.Y > 40 && point.Y < 45 && y < point.Y) continue;
					}

					/*
					if (point.X == 41 && x == 40) continue;
					if (point.Y == 41 && y == 40) continue;
					*/
					var data = Items[x, y];
					if (data.Entity != null) continue;

					if (currAttackPower <= data.AttackPower) continue;

					maxPoint = new Vec2Int(x, y);
					findBetter = true;
					currAttackPower = data.AttackPower;
				}
			}
			
			return findBetter ? maxPoint : null;
		}

		/// <summary>
		/// Находится ли тип под моей атакой
		/// </summary>
		/// <param name="attacker"></param>
		/// <param name="findEntityType"></param>
		/// <returns></returns>
		public bool HasMyEntityTypeUnderAttack(Entity attacker, EntityType findEntityType)
		{
			var x = attacker.Position.X;
			var y = attacker.Position.Y;
			var attackRange = PlayerView.EntityProperties[attacker.EntityType].Attack?.AttackRange ?? 0;

			if (!IsValid(x, y)) return false;
			for (var x1 = x - attackRange; x1 <= x + attackRange; x1++)
			{
				for (var y1 = y - attackRange; y1 < y + attackRange; y1++)
				{
					if (!IsValid(x1, y1) || (y1 == y && x1 == y)) continue;
					if (MyUtils.Dist(x,y,x1,y1) > attackRange) continue;

					var item = Items[x1, y1];
					if (!item.Entity.HasValue
						|| item.Entity.Value.PlayerId != attacker.PlayerId
						|| item.Entity.Value.EntityType != findEntityType
						) { 
						continue; 
					}

					return true;
				}
			}

			return false;
		}

		public bool IsFreePoint(int x, int y, EntityType[] typesToIgnore = null, bool removeUnderAttackPoints = false) 
		{
			if (!IsValid(x, y)) return false;
			var itemData = Items[x, y];

			if (removeUnderAttackPoints && itemData.AttackDamage > 0)
				return false;

			if (itemData.Entity == null) return true;

			return (typesToIgnore == null || typesToIgnore.Length == 0) ?
				false	
				: typesToIgnore.Contains(itemData.Entity.Value.EntityType);
		}


		public bool IsBuilderPointOnWayAttackers(int x, int y, bool countNear) {
			if (!IsValid(x, y)) return true;

			return false // || Items[x, y].MyAttackDamage > 0 
				|| Items[x, y].AttackDamage > 0 
				|| (countNear && Items[x, y].AttackNear > 0)
				//|| (countNear && Items[x, y].MyAttackNear > 0)
				;
		}

		public bool IsBuilderPointOnWayMyAttackers(int x, int y, bool countNear)
		{
			if (!IsValid(x, y)) return true;

			return Items[x, y].MyAttackDamage > 0
				|| (countNear && Items[x, y].MyAttackNear > 0);
		}


		public List<MapItem> GetItemsAround(Vec2Int p) {
			var res = new List<MapItem>();
			if (IsValid(p.X + 1, p.Y)) res.Add(Items[p.X + 1, p.Y]);
			if (IsValid(p.X - 1, p.Y)) res.Add(Items[p.X - 1, p.Y]);
			if (IsValid(p.X, p.Y+1)) res.Add(Items[p.X, p.Y+1]);
			if (IsValid(p.X, p.Y-1)) res.Add(Items[p.X + 1, p.Y-1]);

			return res;
		}

		public void MarkItemAsActionTarget(Vec2Int target) {
			if (!IsValid(target.X, target.Y)) return;
			Items[target.X, target.Y].IsActionTarget = true;
		}

		public void MoveItem(Vec2Int from, Vec2Int to) {
			if (!IsValid(from.X, from.Y)) return;
			if (!IsValid(to.X, to.Y)) return;

			Items[to.X, to.Y].Entity = Items[from.X, from.Y].Entity;
			Items[to.X, to.Y].IsActionTarget = true;

			Items[from.X, from.Y].Entity = null;
		}

		public bool HasEntityAround(Vec2Int p, EntityType entType, bool emptyActionTarget = false) {
			
			if (IsValid(p.X + 1, p.Y) && Items[p.X + 1, p.Y].Entity?.EntityType == entType && (!emptyActionTarget || Items[p.X + 1, p.Y].IsActionTarget == false))
				return true;
			if (IsValid(p.X - 1, p.Y) && Items[p.X - 1, p.Y].Entity?.EntityType == entType && (!emptyActionTarget || Items[p.X - 1, p.Y].IsActionTarget == false))
				return true;
			if (IsValid(p.X, p.Y + 1) && Items[p.X, p.Y + 1].Entity?.EntityType == entType && (!emptyActionTarget || Items[p.X, p.Y+1].IsActionTarget == false))
				return true;
			if (IsValid(p.X, p.Y - 1) && Items[p.X, p.Y - 1].Entity?.EntityType == entType && (!emptyActionTarget || Items[p.X, p.Y-1].IsActionTarget == false))
				return true;

			return false;
		}

		public bool HasEmptyPointArround(int x, int y, int? selfEntityId, bool noDamage)
		{
			if (!IsValid(x, y)) return false;
			for (var x1 = x - 1; x1 <= x + 1; x1++)
			{
				for (var y1 = y - 1; y1 < y + 1; y1++)
				{
					if (!IsValid(x1, y1) || (y1 == y && x1 == y) || (y1 != y && x1 != y)) continue;
					var item = Items[x1, y1];
					if (selfEntityId.HasValue && item.Entity?.Id == selfEntityId.Value) {
						if (noDamage && (item.AttackDamage > 0) || (item.AttackNear > 0)) continue;
						return true;
					}

					if (!item.Entity.HasValue)
					{
						if (noDamage && (item.AttackDamage > 0) || (item.AttackNear > 0)) continue;
						return true;
					}
				}
			}

			return false;
		}

		
		public bool IsValid(int x, int y) {
			if (x < 0 || x >= PlayerView.MapSize) return false;
			if (y < 0 || y >= PlayerView.MapSize) return false;
			return true;
		}

		private void SetEntityAttack(ref Entity ent)
		{


			if (ent.EntityType == EntityType.Turret)
			{
				SetTurretAttack(ref ent);
				return;
			}

			if (!PlayerView.EntityProperties[ent.EntityType].Attack.HasValue
				|| ent.EntityType == EntityType.BuilderUnit
				)
			{
				return;
			}

			var my = ent.PlayerId == PlayerView.MyId;
			var entData = Items[ent.Position.X, ent.Position.Y];

			var attackRange = PlayerView.EntityProperties[ent.EntityType].Attack.Value.AttackRange;
			var damage = PlayerView.EntityProperties[ent.EntityType].Attack.Value.Damage;
			var health = ent.Health;
			var nearDist = 3;

			//for (var x = ent.Position.X - attackRange - nearDist; x < ent.Position.X + attackRange + nearDist; x++)
			for (var x = ent.Position.X - ScanDist; x < ent.Position.X + ScanDist; x++)
			{
				//for (var y = ent.Position.Y - attackRange - nearDist; y < ent.Position.Y + attackRange + nearDist; y++)
				for (var y = ent.Position.Y - ScanDist; y < ent.Position.Y + ScanDist; y++)
				{
					if (!IsValid(x, y)) continue;

					var dist = MyUtils.Dist(ent.Position, new Vec2Int(x, y));
					var itemData = Items[x, y];
					
					if (dist > ScanDist) continue;

					if (ent.EntityType == EntityType.MeleeUnit || ent.EntityType == EntityType.RangedUnit || ent.EntityType == EntityType.Turret)
					{
						if (my) 
						{
							itemData.MyCroudIndex += ScanDist - dist + 1;
						}
						else
						{
							itemData.AttackCroudIndex += ScanDist - dist + 1;
						}
					} 

					if (dist > attackRange + nearDist) continue; 
					var attackPower = 0.0;
					var nearStep = System.Math.Min(nearDist + 1, nearDist - (dist - attackRange) + 1);
					switch (ent.EntityType)
					{
						case EntityType.RangedUnit:
							attackPower = health * damage * nearStep;
							break;
						case EntityType.MeleeUnit:
							attackPower = health * damage * nearStep * 0.8;
							break;
						case EntityType.Turret:
							if (dist > attackRange + 2) nearStep = 0;
							attackPower = health * damage * nearStep * 0.5; 
							break;
					}

					if (dist > attackRange)
					{
						if (itemData.Entity.HasValue 
							&& itemData.Entity.Value.PlayerId != ent.PlayerId 
							&& itemData.Entity.Value.EntityType != EntityType.Resource)
						{
							if (!MySett.AttackerTypes.Contains(itemData.Entity.Value.EntityType))
							{
								entData.AttackToNearPeacefull.Add(itemData.Entity.Value);
							}
							else
							{
								entData.AttackToNear.Add(itemData.Entity.Value);
								itemData.AttackedByNear.Add(ent);
							}
						}

						if (my)
						{
							itemData.MyAttackNear += damage;
							itemData.MyAttackPower += attackPower;
						}
						else
						{
							itemData.AttackNear += damage;
							itemData.AttackPower += attackPower;
						}
					}
					else if (dist <= attackRange)
					{
						if (itemData.Entity.HasValue 
							&& itemData.Entity.Value.PlayerId != ent.PlayerId 
							&& itemData.Entity.Value.EntityType != EntityType.Resource)
						{
							if (!MySett.AttackerTypes.Contains(itemData.Entity.Value.EntityType))
							{
								entData.AttackToPeacefull.Add(itemData.Entity.Value);
							}
							else
							{
								entData.AttackTo.Add(itemData.Entity.Value);
								itemData.AttackedBy.Add(ent);
							}
						}

						if (my)
						{
							itemData.MyAttackDamage += damage;
							itemData.MyAttackHealth += health;
							itemData.MyAttackPower += attackPower;
						}
						else
						{
							itemData.AttackDamage += damage;
							itemData.AttackHealth += health;
							itemData.AttackPower += attackPower;
						}
					}
				}
			}

			
		}


		private void SetTurretAttack(ref Entity ent)
		{
			if (!ent.Active)
			{
				return;
			}

			var allTurretPoints = new Vec2Int[] {
				ent.Position,
				new Vec2Int(ent.Position.X, ent.Position.Y + 1),
				new Vec2Int(ent.Position.X + 1, ent.Position.Y + 1),
				new Vec2Int(ent.Position.X + 1, ent.Position.Y),
			};

			var my = ent.PlayerId == PlayerView.MyId;
			var entData = Items[ent.Position.X, ent.Position.Y];

			var attackRange = PlayerView.EntityProperties[ent.EntityType].Attack.Value.AttackRange;
			var damage = PlayerView.EntityProperties[ent.EntityType].Attack.Value.Damage;
			var health = ent.Health;
			var nearDist = 2;

			var turrentBuildersNear = 0;
			for (var x = ent.Position.X - 2; x < ent.Position.X + 4; x++)
			{
				//for (var y = ent.Position.Y - attackRange - nearDist; y < ent.Position.Y + attackRange + nearDist; y++)
				for (var y = ent.Position.Y - 2; y < ent.Position.Y + 4; y++)
				{
					if (!IsValid(x, y)) continue;

					var itemData = Items[x, y];
					if (itemData.Entity != null && itemData.Entity.Value.EntityType == EntityType.BuilderUnit)
						turrentBuildersNear++;
				}
			}

			health = health + (turrentBuildersNear*13);

			//for (var x = ent.Position.X - attackRange - nearDist; x < ent.Position.X + attackRange + nearDist; x++)
			for (var x = ent.Position.X - ScanDist; x < ent.Position.X + ScanDist + 1; x++)
			{
				//for (var y = ent.Position.Y - attackRange - nearDist; y < ent.Position.Y + attackRange + nearDist; y++)
				for (var y = ent.Position.Y - ScanDist; y < ent.Position.Y + ScanDist + 1; y++)
				{
					if (!IsValid(x, y)) continue;

					var dist = allTurretPoints.Min(t=> MyUtils.Dist(t, new Vec2Int(x, y)));
					var itemData = Items[x, y];

					if (dist > ScanDist) continue;

					if (ent.EntityType == EntityType.MeleeUnit || ent.EntityType == EntityType.RangedUnit || ent.EntityType == EntityType.Turret)
					{
						if (my)
						{
							itemData.MyCroudIndex += ScanDist - dist + 1;
						}
						else
						{
							itemData.AttackCroudIndex += ScanDist - dist + 1;
						}
					}

					if (dist > attackRange + nearDist) continue;
					var attackPower = 0.0;
					var nearStep = System.Math.Min(nearDist + 1, nearDist - (dist - attackRange) + 1);
					switch (ent.EntityType)
					{
						case EntityType.RangedUnit:
							attackPower = health * damage * nearStep;
							break;
						case EntityType.MeleeUnit:
							attackPower = health * damage * nearStep * 0.8;
							break;
						case EntityType.Turret:
							if (dist > attackRange + 1) nearStep = 0;
							attackPower = health * damage * nearStep * 0.5;
							break;
					} 

					if (dist > attackRange)
					{
						if (itemData.Entity.HasValue
							&& itemData.Entity.Value.PlayerId != ent.PlayerId
							&& itemData.Entity.Value.EntityType != EntityType.Resource)
						{
							if (!MySett.AttackerTypes.Contains(itemData.Entity.Value.EntityType))
							{
								entData.AttackToNearPeacefull.Add(itemData.Entity.Value);
							}
							else
							{
								entData.AttackToNear.Add(itemData.Entity.Value);
								itemData.AttackedByNear.Add(ent);
							}
						}

						if (my)
						{
							itemData.MyAttackNear += damage;
							itemData.MyAttackPower += attackPower;
						}
						else
						{
							itemData.AttackNear += damage;
							itemData.AttackPower += attackPower;
						}
					}
					else if (dist <= attackRange)
					{
						if (itemData.Entity.HasValue
							&& itemData.Entity.Value.PlayerId != ent.PlayerId
							&& itemData.Entity.Value.EntityType != EntityType.Resource)
						{
							if (!MySett.AttackerTypes.Contains(itemData.Entity.Value.EntityType))
							{
								entData.AttackToPeacefull.Add(itemData.Entity.Value);
							}
							else
							{
								entData.AttackTo.Add(itemData.Entity.Value);
								itemData.AttackedBy.Add(ent);
							}
						}

						if (my)
						{
							itemData.MyAttackDamage += damage;
							itemData.MyAttackHealth += health;
							itemData.MyAttackPower += attackPower;
						}
						else
						{
							itemData.AttackDamage += damage;
							itemData.AttackHealth += health;
							itemData.AttackPower += attackPower;
						}
					}
				}
			}


		}

		public Vec2Int GetBuilderPointToSaveFromAttack(Vec2Int point, Vec2Int? targetPoint)
		{
			//TODO: Пересчитывать позиции на карте по мере расчетного перемещения юнитов
			if (!IsValid(point.X, point.Y)) return point; 
			var currAttackDamage = Items[point.X, point.Y].AttackDamage;
			var currNearAttackDamage = Items[point.X, point.Y].AttackNear;
			if (currAttackDamage == 0 && currNearAttackDamage == 0) return point;

			if (targetPoint.HasValue && targetPoint.Value.X == 0 && targetPoint.Value.Y == 0)
			{
				targetPoint = null;
			}

			var minPoint = point;

			var points = new List<Vec2Int>();

			for (var x = point.X - 1; x <= point.X + 1; x++)
			{
				for (var y = point.Y - 1; y <= point.Y + 1; y++)
				{
					if (!IsValid(x, y)) continue;
					//По диагонали не ходим
					if (x != point.X && y != point.Y) continue;

					points.Add(new Vec2Int(x, y));


				}
			}

			if (targetPoint.HasValue)
			{
				points = points.OrderBy(p => MyUtils.Dist(p, targetPoint.Value)).ToList();
			}

			foreach (var p in points)
			{
				var data = Items[p.X, p.Y];
				if (data.Entity != null) continue;

				if (currAttackDamage < data.AttackDamage) continue;

				if (currAttackDamage > data.AttackDamage)
				{
					minPoint = p;
					currAttackDamage = data.AttackDamage;
					currNearAttackDamage = data.AttackNear;
					continue;
				}

				if (currNearAttackDamage < data.AttackNear) continue;

				if (currNearAttackDamage > data.AttackNear)
				{
					minPoint = p;
					currAttackDamage = data.AttackDamage;
					currNearAttackDamage = data.AttackNear;
					continue;
				}
			}

			return minPoint;
		}

		/// <summary>
		/// Скрываемся от своих же
		/// </summary>
		/// <param name="point"></param>
		/// <param name="targetPoint"></param>
		/// <returns></returns>
		public Vec2Int GetBuilderPointToSaveFromMyAttack(Vec2Int point, Vec2Int? targetPoint) 
		{
			//TODO: Пересчитывать позиции на карте по мере расчетного перемещения юнитов
			if (!IsValid(point.X, point.Y)) return point;
			var currAttackDamage = Items[point.X, point.Y].MyAttackDamage;
			var currNearAttackDamage = Items[point.X, point.Y].MyAttackNear;
			if (currAttackDamage == 0 && currNearAttackDamage == 0) return point;

			if (targetPoint.HasValue && targetPoint.Value.X == 0 && targetPoint.Value.Y == 0) {
				targetPoint = null;
			}

			var minPoint = point;

			var points = new List<Vec2Int>();

			for (var x = point.X - 1; x <= point.X + 1; x++)
			{
				for (var y = point.Y - 1; y <= point.Y + 1; y++)
				{
					if (!IsValid(x, y)) continue;
					//По диагонали не ходим
					if (x != point.X && y != point.Y) continue;

					points.Add(new Vec2Int(x,y));

					
				}
			}

			if (targetPoint.HasValue) {
				points = points.OrderBy(p=> MyUtils.Dist(p, targetPoint.Value)).ToList();
			}

			foreach (var p in points) 
			{
				var data = Items[p.X, p.Y];
				if (data.Entity != null) continue;

				if (currAttackDamage < data.MyAttackDamage) continue;

				if (currAttackDamage > data.MyAttackDamage)
				{
					minPoint = p;
					currAttackDamage = data.MyAttackDamage;
					currNearAttackDamage = data.MyAttackNear;
					continue;
				}

				if (currNearAttackDamage < data.MyAttackNear) continue;

				if (currNearAttackDamage > data.MyAttackNear)
				{
					minPoint = p;
					currAttackDamage = data.MyAttackDamage;
					currNearAttackDamage = data.MyAttackNear;
					continue;
				}
			}

			return minPoint;
		}

		private void SetEntityPlace(ref Entity ent)
		{
			var size = PlayerView.EntityProperties[ent.EntityType].Size;
			for (var x = ent.Position.X; x < ent.Position.X + size; x++)
			{
				for (var y = ent.Position.Y; y < ent.Position.Y + size; y++)
				{
					if (!IsValid(x, y)) continue;
					Items[x, y].Entity = ent;
				}
			}

			GetMapBlock(ent.Position.X, ent.Position.Y, 10).AllEntities.Add(ent);
			GetMapBlock(ent.Position.X, ent.Position.Y, 20).AllEntities.Add(ent);
			GetMapBlock(ent.Position.X, ent.Position.Y, 40).AllEntities.Add(ent);

			if (
				ent.PlayerId != MySett.MyPlayerId
				&& MySett.AttackerTypes.Contains(ent.EntityType)
				)
			{
				GetMapBlock(ent.Position.X, ent.Position.Y, 10).EnemyAttackers.Add(ent);
				GetMapBlock(ent.Position.X, ent.Position.Y, 20).EnemyAttackers.Add(ent);
				GetMapBlock(ent.Position.X, ent.Position.Y, 40).EnemyAttackers.Add(ent);
			}

			if (
				ent.PlayerId == MySett.MyPlayerId
				&& ent.EntityType == EntityType.BuilderUnit
				)
			{
				GetMapBlock(ent.Position.X, ent.Position.Y, 10).MyBuildersCount++;
				GetMapBlock(ent.Position.X, ent.Position.Y, 20).MyBuildersCount++;
				GetMapBlock(ent.Position.X, ent.Position.Y, 40).MyBuildersCount++;
			}

			if (
				ent.PlayerId == MySett.MyPlayerId
				&& (
					ent.EntityType == EntityType.BuilderBase
					|| ent.EntityType == EntityType.RangedBase
					|| ent.EntityType == EntityType.MeleeBase
					|| ent.EntityType == EntityType.House
				)
				)
			{
				GetMapBlock(ent.Position.X, ent.Position.Y, 10).MyBuildingsCount++;
				GetMapBlock(ent.Position.X, ent.Position.Y, 20).MyBuildingsCount++;
				GetMapBlock(ent.Position.X, ent.Position.Y, 40).MyBuildingsCount++;
			}

			if (
				ent.PlayerId == MySett.MyPlayerId
				&& (MySett.AttackerTypes.Contains(ent.EntityType))
				)
			{
				GetMapBlock(ent.Position.X, ent.Position.Y, 10).MyAttackersCount++;
				GetMapBlock(ent.Position.X, ent.Position.Y, 20).MyAttackersCount++;
				GetMapBlock(ent.Position.X, ent.Position.Y, 40).MyAttackersCount++;
			}
		}

		public List<Vec2Int> GetAllPointsAroundBuilding(Entity baseEntity)
		{
			var size = PlayerView.EntityProperties[baseEntity.EntityType].Size;
			var pos = baseEntity.Position;

			//Перебираем куда можно поставить юнита рядом с базой
			var allPoints = new List<Vec2Int>();
			for (var y = 0; y < size; y++)
			{
				//справа
				if (MySett.Map.IsValid(pos.X + size, pos.Y + y))
					allPoints.Add(new Vec2Int(pos.X + size, pos.Y + y));

				//слева
				if (MySett.Map.IsValid(pos.X - 1, pos.Y + y))
					allPoints.Add(new Vec2Int(pos.X - 1, pos.Y + y));
			}

			for (var x = 0; x < size; x++)
			{
				//сверху
				if (MySett.Map.IsValid(pos.X + x, pos.Y + size))
					allPoints.Add(new Vec2Int(pos.X + x, pos.Y + size));
				//снизу
				if (MySett.Map.IsValid(pos.X + x, pos.Y - 1))
					allPoints.Add(new Vec2Int(pos.X + x, pos.Y - 1));
			}

			return allPoints;
		}

		public List<Vec2Int> GetEmptyPointsAroundBuilding(Entity baseEntity, EntityType[] typesToIgnore, bool removeUnderAttackPoints = true)
		{
			var size = PlayerView.EntityProperties[baseEntity.EntityType].Size;
			var pos = baseEntity.Position;

			//Перебираем куда можно поставить юнита рядом с базой
			var allPoints = new List<Vec2Int>();
			for (var y = 0; y < size; y++)
			{
				//справа
				if (MySett.Map.IsFreePoint(pos.X + size, pos.Y + y, typesToIgnore, removeUnderAttackPoints))
					allPoints.Add(new Vec2Int(pos.X + size, pos.Y + y));

				//слева
				if (MySett.Map.IsFreePoint(pos.X - 1, pos.Y + y, typesToIgnore, removeUnderAttackPoints))
					allPoints.Add(new Vec2Int(pos.X - 1, pos.Y + y));
			}

			for (var x = 0; x < size; x++)
			{
				//сверху
				if (MySett.Map.IsFreePoint(pos.X + x, pos.Y + size, typesToIgnore, removeUnderAttackPoints))
					allPoints.Add(new Vec2Int(pos.X + x, pos.Y + size));
				//снизу
				if (MySett.Map.IsFreePoint(pos.X + x, pos.Y - 1, typesToIgnore, removeUnderAttackPoints))
					allPoints.Add(new Vec2Int(pos.X + x, pos.Y - 1));
			}

			return allPoints;
		}
	}

	public class MapItem
	{
		public bool IsActionTarget { get; set; }
		public Entity? Entity { get; set; }
		public int AttackDamage { get; set; }

		public int AttackNear { get; set; }

		public int AttackHealth { get; set; }

		public double AttackPower { get; set; }

		public int AttackCroudIndex { get; set; }

		public int MyAttackDamage { get; set; }

		public int MyAttackNear { get; set; }

		public int MyAttackHealth { get; set; }

		public double MyAttackPower { get; set; }

		public int MyCroudIndex { get; set; }

		public List<Entity> AttackedBy { get; set; } = new List<Entity>();

		public List<Entity> AttackedByNear { get; set; } = new List<Entity>();

		public List<Entity> AttackTo { get; set; } = new List<Entity>();
		public List<Entity> AttackToNear { get; set; } = new List<Entity>();

		public List<Entity> AttackToPeacefull { get; set; } = new List<Entity>();
		public List<Entity> AttackToNearPeacefull { get; set; } = new List<Entity>();
		
	}
	public class MapBlock
	{
		public MapBlock() { }
		public MapBlock(int num, int lvl, int x, int y, int size) {
			Num = num;
			Lvl = lvl;
			Position = new Vec2Int(x*size, y*size);
			Size = size;
		}

		public int Num { get; set; }
		public int Lvl { get; set; }
		public Vec2Int Position { get; set; }
		public int Size { get; set; }
		public MapItem SumData { get; set; } = new MapItem();

		public List<MapItem> AllMapItems = new List<MapItem>();
		public List<Entity> AllEntities = new List<Entity>();
		public List<Entity> EnemyAttackers = new List<Entity>();

		public int MyBuildersCount;
		public int MyBuildingsCount;
		public int MyAttackersCount;
	}
}
