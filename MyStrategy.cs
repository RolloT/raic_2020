using aicup2020;
using Aicup2020.Model;
using System.Collections.Generic;
using System.Linq;
using static aicup2020.Needs;

namespace Aicup2020
{
    public partial class MyStrategy
    {
        public Needs Needs = new Needs();
        public List<Need> NeedsList = new List<Need>();
        //public List<UnitGroup> UnitGroups = new List<UnitGroup>(); 

        public PlayerView PlayerView;
        public DebugInterface DebugInterface;
        public HashSet<string> CreateNewPositions = new HashSet<string>();

        public bool AttackerCollectPoint = true;

        public Action GetAction(PlayerView playerView, DebugInterface debugInterface)
        {
            MySett.InitMySett(ref playerView);

            PlayerView = playerView;
            DebugInterface = debugInterface;

            Needs.PlayerView = PlayerView;
            Needs.DebugInterface = DebugInterface;

            var res = new Action(new Dictionary<int, EntityAction>(0));

            ClearAllbaseActions(ref res);
            FireAllTurretsActions(ref res);

            var newNeeds = Needs.CalcNeeds().OrderBy(n=>n.Priority);

            var totalResources = MySett.MyPlayer.Resource;
            var lastResources = totalResources; 

            //���� �� ������������ � ������� �����������
            foreach (var need in newNeeds)
            {
                //�� ������ ������� ���� ���� ������� �����������
                if (need.OneEntityResources > 0 && lastResources < need.OneEntityResources)
                {
                    lastResources -= need.OneEntityResources;
                    continue;
                }

                lastResources -= (need.Count * need.OneEntityResources);

                switch (need.NeedType)
                {
                    case NeedTypes.CreateMelee:
                    case NeedTypes.CreateRanged:
                        TryToBornAttacker(ref res, need.Target.Value, need.EntityType);
                        break;
                    case NeedTypes.BuildBuilderBase:
                    case NeedTypes.BuildMeleeBase:
                    case NeedTypes.BuildRangedBase:
                    case NeedTypes.BuildHouse:
                    case NeedTypes.BuildTurret:
                        for (var i = 0; i < need.Count; i++)
                        {
                            CreateBuildAction(ref res, need.EntityType);
                        }
                        break;
                    case NeedTypes.CreateBuilder:
                        for (var i = 0; i < need.Count; i++)
                        {
                            TryToBornBuilder(ref res);
                        }
                        break;
                    case NeedTypes.Repair:
                        for (var i = 0; i < need.Count; i++)
                        {
                            TryToRepairBfs(ref res, need.Target.Value);
                        }
                        break;
                }
            }

            CheckCornersActions(ref res);
            SetSaboteurs(ref res);
            CreateStandartCollectResourcesActions(ref res);
            //DistribBuilderDefendersByMapBlocks(ref res); 
            DistribAttackersByMapBlocks(ref res);
            
            CreateStandartAttackActions(ref res);

            return res; 
        }

        private List<Entity> _emptyEntityList = new List<Entity>();

        public void ClearAllbaseActions(ref Action action)
        {
            foreach (var b in MySett.GetMyEntities(MySett.BuildingTypes, false).ToList())
            {
                action.EntityActions[b.Id] =
                    new EntityAction
                    {
                        MoveAction = null,
                        BuildAction = null,
                        RepairAction = null,
                        AttackAction = null
                    };
            }
        }

        public void FireAllTurretsActions(ref Action action)
        {
            foreach (var b in MySett.GetMyEntities(EntityType.Turret, false))
            {
                action.EntityActions[b.Id] =
                    new EntityAction
                    {
                        MoveAction = null,
                        BuildAction = null,
                        RepairAction = null,
                        AttackAction = new AttackAction
                        {
                            Target = null,
                            AutoAttack = new AutoAttack
                            {
                                PathfindRange = PlayerView.MaxPathfindNodes,// sightRange,
                                ValidTargets = System.Array.Empty<EntityType>() // new[] { EntityType.Resource }
                            }
                        }
                    };
            }
        }

        public static EntityType[] TypesToAttackFirst  = new[] { EntityType.BuilderUnit, EntityType.MeleeBase, EntityType.RangedBase, EntityType.House };
        public static EntityType[] TypesToAttackSecond = new[] { EntityType.MeleeUnit, EntityType.RangedUnit };
        public static EntityType[] TypesToAttackThird = new[] { EntityType.BuilderBase, EntityType.Turret };

        public static EntityType[] TypesToAttackNoBuilders = new[] { EntityType.BuilderBase, EntityType.RangedBase, 
            EntityType.MeleeBase, EntityType.MeleeUnit, 
            EntityType.RangedUnit, EntityType.Turret, EntityType.House };

        public bool FirtsWaitingBatch = true;

        public void CheckCornersActions(ref Action action)
        {
            var mapData = MySett.Map.Items[PlayerView.MapSize - 8, 8];
            if (mapData.Entity?.PlayerId == MySett.MyPlayerId)
			{
                MySett.IsRightConnerClear = true;
                action.EntityActions[mapData.Entity.Value.Id] = CreateStayInCornerAction(mapData.Entity.Value.Id);
            }

            mapData = MySett.Map.Items[8, PlayerView.MapSize - 8];
            if (mapData.Entity?.PlayerId == MySett.MyPlayerId)
            {
                MySett.IsLeftConnerClear = true;
                action.EntityActions[mapData.Entity.Value.Id] = CreateStayInCornerAction(mapData.Entity.Value.Id);
            }

            mapData = MySett.Map.Items[PlayerView.MapSize - 8, PlayerView.MapSize - 8];
            if (mapData.Entity?.PlayerId == MySett.MyPlayerId)
            {
                MySett.IsFarConnerClear = true;
                action.EntityActions[mapData.Entity.Value.Id] = CreateStayInCornerAction(mapData.Entity.Value.Id);
            }
        }

        public EntityAction CreateStayInCornerAction(int entityId) {
            MySett.MyEntityWithActionIds.Add(entityId);

            return new EntityAction
            {
                MoveAction = null,
                BuildAction = null,
                RepairAction = null,
                AttackAction = new AttackAction
                {
                    Target = null,
                    AutoAttack = new AutoAttack
                    {
                        PathfindRange = 10, //(int)(move_action.HasValue? (MyUtils.Dist(move_action.Value.Target, entity.Position) * 4) : PlayerView.MaxPathfindNodes),//props.SightRange, //waitingBatch ? 5 : props.SightRange,
                        ValidTargets = System.Array.Empty<EntityType>() //Map.HasMyEntityTypeUnderAttack(entity, EntityType.BuilderUnit) ? TypesToAttackNoBuilders : System.Array.Empty<EntityType>() // new[] { EntityType.Resource }
                    }
                }
            };
        }

        

        /// <summary>
        /// ������������ ���������� �� ����� � ������������ (3 ��� �� ����)
        /// </summary>
        /// <param name="action"></param>
        public void DistribBuilderDefendersByMapBlocks(ref Action action)
		{
            List<MapBlock> blocks = null;
            var minDefenders = 2;
            blocks = MySett.Map.MapBlocks10.Where(b =>
                b.MyBuildersCount > 7
                && b.MyAttackersCount < minDefenders
            ).ToList();


            var sumBlocksAttackersRequired = blocks.Sum(b => minDefenders - b.MyAttackersCount);
            var allAttackers = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes, true);
            blocks = blocks.OrderByDescending(b => System.Math.Max(b.Position.X, b.Position.Y)).ThenBy(b => b.MyAttackersCount).ToList();
            var totalAttackers = allAttackers.Count();
            foreach (var block in blocks)
            {
                var myUnitsForBlockCount = minDefenders - block.MyAttackersCount;

                var myAttackers = allAttackers
                    //.Where(e=>MyUtils.Dist(e.Position, block.Position) < PlayerView.MapSize * 1.5)
                    .OrderBy(e => MyUtils.Dist(e.Position.X, e.Position.Y, block.Position.X + block.Size / 2, block.Position.Y + block.Size / 2))
                    .Take(myUnitsForBlockCount).ToList();

                foreach (var attacker in myAttackers)
                {
                    MySett.MyEntityWithActionIds.Add(attacker.Id);
                    allAttackers.RemoveAll(a => a.Id == attacker.Id);
                    MoveAction? moveAction = null;
                    bool noAutoAttack = false;
                    bool needMove = true;

                    moveAction = GetAttackDefenceAction(attacker);
                    if (moveAction != null)
                    {
                        noAutoAttack = true;
                        needMove = false;
                    }

                    if (needMove)
                    {
                        moveAction = new MoveAction
                        {
                            Target = new Vec2Int(block.Position.X + block.Size / 2, block.Position.Y + block.Size / 2),
                            BreakThrough = false,
                            FindClosestPosition = true
                        };
                    }


                    action.EntityActions[attacker.Id] = new EntityAction
                    {
                        MoveAction = moveAction,
                        AttackAction = new AttackAction
                        {
                            AutoAttack = noAutoAttack ? null : new AutoAttack
                            {
                                PathfindRange = 8,
                                ValidTargets = System.Array.Empty<EntityType>() //Map.HasMyEntityTypeUnderAttack(attacker, EntityType.BuilderUnit) ? TypesToAttackNoBuilders :  new[] { EntityType.Resource }
                            }
                        }
                    };
                }
            }
        }

        public AttackAction? GetAttackAction(Entity ent, Dictionary<int, int> attackToHealth)
        {
            if (!PlayerView.EntityProperties[ent.EntityType].Attack.HasValue) return null;

            //AttackAction? res = null;
            var canAttackTo = MySett.Map.Items[ent.Position.X, ent.Position.Y].AttackTo;
            // if (canAttackTo == null) return null;

            //turrets 
            foreach (var t in canAttackTo.Where(e => e.EntityType == EntityType.Turret)
                    .OrderBy(e => e.Health)
                    .ThenByDescending(e => MyUtils.Dist(ent.Position, e.Position)).ThenBy(e => e.Id)
                    )
            {
                var res = GetAttackActionOnTarget(ent, t, attackToHealth);
                if (res != null) 
                    return res;
            }
            //melles
            foreach (var t in canAttackTo.Where(e=>e.EntityType == EntityType.MeleeUnit)
                    .OrderBy(e=>e.Health)
                    .ThenByDescending(e=> MyUtils.Dist(ent.Position, e.Position))
                    )
            {
                if (!attackToHealth.ContainsKey(t.Id) || attackToHealth[t.Id] > 0) {
                    var res = GetAttackActionOnTarget(ent, t, attackToHealth);
                    if (res != null) 
                        return res;
                }
            }
            //ranged
            foreach (var t in canAttackTo.Where(e => e.EntityType == EntityType.RangedUnit)
                    .OrderBy(e => e.Health)
                    .ThenByDescending(e => MyUtils.Dist(ent.Position, e.Position)).ThenBy(e => e.Id)
                    )
            {
                if (!attackToHealth.ContainsKey(t.Id) || attackToHealth[t.Id] > 0)
                {
                    var res = GetAttackActionOnTarget(ent, t, attackToHealth);
                    if (res != null) 
                        return res;
                }
            }
            //BuilderUnit
            foreach (var t in canAttackTo.Where(e => e.EntityType == EntityType.BuilderUnit)
                    .OrderBy(e => e.Health)
                    .ThenByDescending(e => MyUtils.Dist(ent.Position, e.Position)).ThenBy(e => e.Id)
                    )
            {
                if (!attackToHealth.ContainsKey(t.Id) || attackToHealth[t.Id] > 0)
                {
                    var res = GetAttackActionOnTarget(ent, t, attackToHealth);
                    if (res != null) 
                        return res;
                }
            }
            //House 
            foreach (var t in canAttackTo.Where(e => e.EntityType == EntityType.House)
                    .OrderBy(e => e.Health)
                    .ThenByDescending(e => MyUtils.Dist(ent.Position, e.Position)).ThenBy(e => e.Id)
                    )
            {
                if (!attackToHealth.ContainsKey(t.Id) || attackToHealth[t.Id] > 0)
                {
                    var res = GetAttackActionOnTarget(ent, t, attackToHealth);
                    if (res != null) return res;
                }
            }
            //BuilderBase
            foreach (var t in canAttackTo.Where(e => e.EntityType == EntityType.BuilderBase)
                    .OrderBy(e => e.Health)
                    .ThenByDescending(e => MyUtils.Dist(ent.Position, e.Position)).ThenBy(e => e.Id)
                    )
            {
                if (!attackToHealth.ContainsKey(t.Id) || attackToHealth[t.Id] > 0)
                {
                    var res = GetAttackActionOnTarget(ent, t, attackToHealth);
                    if (res != null) return res;
                }
            }

            //RangedBase
            foreach (var t in canAttackTo.Where(e => e.EntityType == EntityType.RangedBase)
                    .OrderBy(e => e.Health)
                    .ThenByDescending(e => MyUtils.Dist(ent.Position, e.Position)).ThenBy(e => e.Id)
                    )
            {
                if (!attackToHealth.ContainsKey(t.Id) || attackToHealth[t.Id] > 0)
                {
                    var res = GetAttackActionOnTarget(ent, t, attackToHealth);
                    if (res != null) return res;
                }
            }

            //MeleeBase
            foreach (var t in canAttackTo.Where(e => e.EntityType == EntityType.MeleeBase)
                    .OrderBy(e => e.Health)
                    .ThenByDescending(e => MyUtils.Dist(ent.Position, e.Position)).ThenBy(e => e.Id)
                    )
            {
                if (!attackToHealth.ContainsKey(t.Id) || attackToHealth[t.Id] > 0)
                {
                    var res = GetAttackActionOnTarget(ent, t, attackToHealth);
                    if (res != null) return res;
                }
            }

            return null;
        }



        public AttackAction? GetAttackActionOnTarget(Entity ent, Entity t, Dictionary<int, int> attackToHealth)
        {
            if (!attackToHealth.ContainsKey(t.Id) || attackToHealth[t.Id] > 0)
            {
                var attackToHealthVal = attackToHealth.ContainsKey(t.Id) ? attackToHealth[t.Id] : t.Health;
                attackToHealth[t.Id] = attackToHealthVal - PlayerView.EntityProperties[ent.EntityType].Attack.Value.Damage;
                return new AttackAction(t.Id, null);
            }
            return null;
        }

        /// <summary>
        /// ���� �� ���������� �������
        /// </summary>
        /// <param name="targets"></param>
        /// <param name="entityPos"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        private BfsResult FindNearestResourcePath(Vec2Int entityPos)
        {
            var bfs = new Bfs();
            var bfsHelp = new BfsFindResourceHelper();
            var path = bfs.FindFirstNode(entityPos, bfsHelp.IsValidPos, bfsHelp.IsTarget, 60); 
            return path;
        }

        public EntityType[] BuildFarFromHomeTypes = new EntityType[] { EntityType.MeleeBase, EntityType.RangedBase, EntityType.Turret, EntityType.Wall };

        public ToBuildData FindFreeBuilderToBuild(EntityType buildingType) 
        {
            var buildingSize = PlayerView.EntityProperties[buildingType].Size;
            List<Entity> allBuilders;
            if (buildingType == EntityType.Turret) 
            {
                var allMyTurrents = MySett.GetMyEntities(EntityType.Turret);
                var minTurretDist = System.Math.Max(11, (20.0 / (double)allMyTurrents.Count));
                allBuilders = MySett.GetMyEntities(EntityType.BuilderUnit, true).
                    Where(e=> !allMyTurrents.Any(t=>MyUtils.Dist(t.Position, e.Position) < minTurretDist)
                    && MySett.Map.Items[e.Position.X, e.Position.Y].AttackCroudIndex == 0
                    && e.Position.X > 5 && e.Position.Y > 5) 
                    .OrderByDescending(e => System.Math.Max(e.Position.X, e.Position.Y)).ThenBy(e => e.Id).ToList();
            }
            else
            {
                allBuilders = BuildFarFromHomeTypes.Contains(buildingType) ?
                    MySett.GetMyEntities(EntityType.BuilderUnit, true).Where(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackCroudIndex == 0).OrderByDescending(e => e.Position.X + e.Position.Y).ThenBy(e => e.Id).ToList()
                    : MySett.GetMyEntities(EntityType.BuilderUnit, true).Where(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackCroudIndex == 0).OrderBy(e => e.Position.X + e.Position.Y).ThenBy(e => e.Id).ToList();
            }


            foreach (var b in allBuilders)
            {
                var point = FindPointToBuild(b, buildingSize, buildingType);
                if (point != null)
                {
                    return new ToBuildData { 
                        Builder = b,
                        BuildPoint = point.Value
                    };
                }
            }

            return null;
        }

        /// <summary>
        /// ���� ����� ��� ����� ���-�� ��������� ����� � ������� + ����� �� ���������� �������
        /// </summary>
        /// <param name="builderPos"></param>
        /// <param name="buildingSize"></param>
        /// <returns></returns>
        public Vec2Int? FindPointToBuild(Entity builder, int buildingSize, EntityType buildingType)
        {
            //���������� ���� ����� ��������� ��������, ����� ���� ����� �� ����������
            var allPoints = new List<Vec2Int>();
            for (var y = 0; y < buildingSize; y++)
            {
                //������
                allPoints.Add(new Vec2Int(builder.Position.X + 1, builder.Position.Y - y));
                allPoints.Add(new Vec2Int(builder.Position.X - buildingSize, builder.Position.Y - y));
            }

            for (var x = 0; x < buildingSize; x++) 
            {
                //������
                allPoints.Add(new Vec2Int(builder.Position.X - x, builder.Position.Y + 1));
                //�����
                allPoints.Add(new Vec2Int(builder.Position.X - x, builder.Position.Y - buildingSize));
            }
            allPoints = allPoints.OrderBy(p => p.X + p.Y).ToList();

            foreach (var pointToTry in allPoints)
            {
                if (!(IsCrossAnyToBuild(pointToTry, buildingSize, buildingType, builder.Position))) 
                    return pointToTry; 
            }

            return null;
        }

        public static EntityType[] entitiesToLeaveSpaceBetween = new[] { EntityType.BuilderBase, EntityType.House, EntityType.MeleeBase, EntityType.RangedBase, EntityType.Turret };

        /// <summary>
        /// ������������ �� ���� � ����� �� ������
        /// </summary>
        /// <param name="entries"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public bool IsCrossAnyToBuild(Vec2Int pos, int size, EntityType buildingType, Vec2Int builderPosition, bool canBeMovedAway = false) 
        {
            
            if (
                !MySett.Map.IsValid(pos.X, pos.Y)
                || !MySett.Map.IsValid(pos.X + size - 1, pos.Y + size - 1)
                )
            {
                return true;
            }

            var leaveSpace = entitiesToLeaveSpaceBetween.Contains(buildingType);

            for (var x = pos.X - 1; x <= pos.X + size; x++)
            {
                for (var y = pos.Y - 1; y <= pos.Y + size; y++)
                {
                    if (!MySett.Map.IsValid(x,y)) continue;
                    if (MySett.Map.Items[x, y].AttackDamage > 0 || MySett.Map.Items[x, y].AttackNear > 0) continue;

                    if (builderPosition.X == x && builderPosition.Y == y)
                        continue;

                    var ent = MySett.Map.Items[x, y].Entity;
                    if (ent == null) continue;
                    if (ent.Value.PlayerId == MySett.MyPlayerId && PlayerView.EntityProperties[ent.Value.EntityType].CanMove && canBeMovedAway) {
                        //TODO: ����� ��������� ��������� �����
                        continue; 
                    }

                    if ((ent.Value.EntityType == EntityType.Resource
                        || ent.Value.EntityType == EntityType.RangedUnit
                        || ent.Value.EntityType == EntityType.BuilderUnit
                        || ent.Value.EntityType == EntityType.MeleeUnit)
                        && (
                        x == pos.X - 1 || x == pos.X + size
                        || y == pos.Y - 1 || y == pos.Y + size
                        )
                    ) 
                    {
                        continue;
                    }

                    return true;
                }
            }

            return false;
        }

        public bool IsPointInsideEntity(Vec2Int point, Vec2Int pos2, int size2)
		{
            return point.X >= pos2.X
                && point.X <= pos2.X + size2-1
                && point.Y >= pos2.Y
                && point.Y <= pos2.Y + size2-1
                ;
		}

        /// <summary>
        /// ������ �����������
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool TryToBornAttacker(ref Action action, Entity target, EntityType attackerType)
        {
            var point = FindEmptyPointNearBuilding(target, true, new Vec2Int(40,40));
            if (point != null)
            {
                CreateBornAction(ref action, target, attackerType, point.Value);
                return true;
            }

            return false;
        }


        public static HashSet<int> EmptyIntHashSet = new HashSet<int>();

        /// <summary>
        /// ������ ���������
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool TryToBornBuilder(ref Action action) 
        {
            //����� ����� ���� ���������� ������� ��� �� ������ ������, ������� �� ������ ���� � ������ ���� ���� ������ ����� ������
            var myBases = MySett.GetMyEntities(EntityType.BuilderBase, true).Where(e => e.Active).OrderByDescending(e=>e.Position.X + e.Position.Y).ThenBy(e => e.Id).ToList();

            if (myBases.Count == 0) return false;
            foreach(var b in myBases){
                Vec2Int? targetResourcePoint = null;
                /*
                if (PlayerView.CurrentTick < 200) 
                {
                    targetResourcePoint = FindNearestResource(EmptyIntHashSet, new Vec2Int(b.Position.X + 2, b.Position.Y + 2), -1).Position;
                }
                */

                var point = FindEmptyPointNearBuilding(b, true, targetResourcePoint);
                if (point != null)
                {
                    CreateBornAction(ref action, b, EntityType.BuilderUnit, point.Value);
                    return true;
                }
            }
            return false;
        }

        //����� ����� ��� ������ ����� ������
        public Vec2Int? FindEmptyPointNearBuilding(Entity baseEntity, bool farFromHome, Vec2Int? targetPos) 
        {
            if (targetPos.HasValue && !(targetPos.Value.X > 0 || targetPos.Value.Y > 0))
			{
                targetPos = null;
            }

            var size = PlayerView.EntityProperties[baseEntity.EntityType].Size;
            var pos = baseEntity.Position;

            //���������� ���� ����� ��������� ����� ����� � �����
            var allPoints = new List<Vec2Int>();
            for (var y = 0; y < size; y++)
            {
                //������
                allPoints.Add(new Vec2Int(pos.X + size, pos.Y + y));
                //�����
                allPoints.Add(new Vec2Int(pos.X - 1, pos.Y + y));
            }

            for (var x = 0; x < size; x++)
            {
                //������
                allPoints.Add(new Vec2Int(pos.X + x, pos.Y + size));
                //�����
                allPoints.Add(new Vec2Int(pos.X + x, pos.Y - 1));
            }

            if (targetPos.HasValue)
            {
                allPoints = allPoints.OrderBy(p => MyUtils.Dist(p, targetPos.Value)).ToList();
            }
            else
            {
                if (farFromHome)
                {
                    allPoints = (PlayerView.CurrentTick % 2 == 0) ? allPoints.OrderByDescending(p => p.Y).ThenByDescending(p => p.X).ToList() : allPoints.OrderByDescending(p => p.Y).ThenBy(p => p.X).ToList();
                }
                else
                {
                    allPoints = (PlayerView.CurrentTick % 2 == 0) ? allPoints.OrderBy(p => p.Y).ThenByDescending(p => p.X).ToList() : allPoints.OrderBy(p => p.Y).ThenBy(p => p.X).ToList();
                }
            }

            foreach (var pointToTry in allPoints)
            {
                if (!MySett.Map.IsFreePoint(pointToTry.X, pointToTry.Y)) continue;
                return pointToTry;
            }

            return null;
        }

        public bool CreateBornAction(ref Action action, Entity baseEntity, EntityType type, Vec2Int point)
        {
            if (!MySett.Map.IsValid(point.X, point.Y))
            {
                return false;
            }

            var build_action = new BuildAction
            {
                EntityType = type,
                Position = new Vec2Int
                {
                    X = point.X,
                    Y = point.Y,
                }
            };

            var entityAction = new EntityAction
            {
                MoveAction = null,
                BuildAction = build_action,
                RepairAction = null,
                AttackAction = null
            };

            action.EntityActions[baseEntity.Id] = entityAction;
            MySett.MyEntityWithActionIds.Add(baseEntity.Id);

            return true;
        }

        /// <summary>
        /// ���� ����� ������� ����� �������� �� �����, ����� ��������� ���
        /// </summary>
        /// <param name="action"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool MoveMyEntityToClearBuildPoint(ref Action action, EntityType buildingType)
        {
            var buildingSize = PlayerView.EntityProperties[buildingType].Size;
            Vec2Int? findPoint = null;
            for (var x = buildingSize; x < PlayerView.MapSize*0.5; x++)
            {
                for (var y = buildingSize; y < PlayerView.MapSize*0.5; y++)
                {
                    var point = FindPointToBuild(new Entity { Position = new Vec2Int(x, y) }, buildingSize, buildingType);
                    if (point != null)
                    {
                        findPoint = new Vec2Int(x, y);
                        break;
                    }
                }
                if (findPoint != null) break;
            }

            if (findPoint != null)
            {
                var nearestBuilder = MySett.GetMyEntities(EntityType.BuilderUnit, true).OrderBy(e => MyUtils.Dist(e.Position, findPoint.Value)).ThenBy(e => e.Id).FirstOrDefault();
                if (nearestBuilder.Id != 0)
                {
                    MySett.MyEntityWithActionIds.Add(nearestBuilder.Id);

                    var move_action = new MoveAction
                    {
                        Target = findPoint.Value,
                        FindClosestPosition = true,
                        BreakThrough = true
                    };

                    action.EntityActions[nearestBuilder.Id] =
                    new EntityAction
                    {
                        MoveAction = move_action,
                        BuildAction = null,
                        RepairAction = null,
                        AttackAction = new AttackAction
                        {
                            Target = null,
                            AutoAttack = null /*new AutoAttack
                            {
                                PathfindRange = PlayerView.MaxPathfindNodes,//props.SightRange,
                                ValidTargets = underAttack ? System.Array.Empty<EntityType>() : new[] { EntityType.Resource, EntityType.BuilderUnit }
                            }*/
                        }
                    };
                }
            }

            return false;
        }

        public Vec2Int GetNextPointFromCorner(Vec2Int p)
        {
            var x = p.X;
            var y = p.Y;
            if (x == y) {
                x++;
                y = 0;
                return new Vec2Int(x, y);
            }

            if (x > y)
            {
                var t = x;
                x = y;
                y = t;
            }
            else
            {
                var t = x;
                x = y;
                y = t;
                y++;
            }

            return new Vec2Int(x, y);
        }

        /// <summary>
        /// ���� ���� ��������� ���������, ����� �� ��� ��������
        /// </summary>
        /// <param name="action"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool MoveBuilderToBuildPoint(ref Action action, EntityType buildingType) 
        {
            var buildingSize = PlayerView.EntityProperties[buildingType].Size;
            Vec2Int? findPoint = null;
            //��� ���������� �� �� ��������� � ���������� �� ����
            var currPoint = new Vec2Int(0, 0); 
            while (currPoint.X < PlayerView.MapSize * 0.5)
            {
                if (currPoint.X < buildingSize || currPoint.Y < buildingSize)
                {
                    currPoint = GetNextPointFromCorner(currPoint);
                    continue;
                }

                var point = FindPointToBuild(new Entity { Position = currPoint }, buildingSize, buildingType);
                if (point != null)
                {
                    findPoint = currPoint;
                    break;
                }
                currPoint = GetNextPointFromCorner(currPoint);
            }

            if (findPoint != null)
            {
                var nearestBuilder = MySett.GetMyEntities(EntityType.BuilderUnit, true).OrderBy(e => MyUtils.Dist(e.Position, findPoint.Value)).ThenBy(e => e.Id).FirstOrDefault();
                if (nearestBuilder.Id != 0)
                {
                    MySett.MyEntityWithActionIds.Add(nearestBuilder.Id);

                    var move_action = new MoveAction
                    {
                        Target = findPoint.Value,
                        FindClosestPosition = true,
                        BreakThrough = true 
                    };

                    action.EntityActions[nearestBuilder.Id] =
                    new EntityAction
                    {
                        MoveAction = move_action,
                        BuildAction = null,
                        RepairAction = null,
                        AttackAction = new AttackAction
                        {
                            Target = null,
                            AutoAttack = null /*new AutoAttack
                            {
                                PathfindRange = PlayerView.MaxPathfindNodes,//props.SightRange,
                                ValidTargets = underAttack ? System.Array.Empty<EntityType>() : new[] { EntityType.Resource, EntityType.BuilderUnit }
                            }*/
                        }
                    };
                }
            }

            return false;
        }

        public bool CreateBuildAction(ref Action action, EntityType type) 
        {
            var props = PlayerView.EntityProperties[type];

            var toBuildData = FindFreeBuilderToBuild(type);
            if (toBuildData == null)
            {
                return MoveBuilderToBuildPoint(ref action, type);
            }

            if (!MySett.Map.IsValid(toBuildData.BuildPoint.X, toBuildData.BuildPoint.Y)
                || !MySett.Map.IsValid(toBuildData.BuildPoint.X - 1 + props.Size, toBuildData.BuildPoint.Y - 1 + props.Size)
                )
            {
                return false;
            }

            var build_action = new BuildAction
            {
                EntityType = type,
                Position = new Vec2Int
                {
                    X = toBuildData.BuildPoint.X,
                    Y = toBuildData.BuildPoint.Y,
                } 
            };

            var entityAction = new EntityAction
            {
                MoveAction = null,
                BuildAction = build_action,
                RepairAction = null,
                AttackAction = new AttackAction
                {
                    Target = null,
                    AutoAttack = new AutoAttack
                    {
                        PathfindRange = PlayerView.MaxPathfindNodes, //PlayerView.MaxPathfindNodes,//props.SightRange,  
                        ValidTargets = System.Array.Empty<EntityType>() // (builder.EntityType == EntityType.BuilderUnit ? new[] { EntityType.Resource } : new EntityType[0])
					}
                }
            };

            action.EntityActions[toBuildData.Builder.Id] = entityAction;
            MySett.MyEntityWithActionIds.Add(toBuildData.Builder.Id);
            return true;
        }

        public void DebugUpdate(PlayerView playerView, DebugInterface debugInterface) 
        {
            debugInterface.Send(new DebugCommand.Clear());
            debugInterface.GetState();
        }

        private bool IsUnit(EntityType entityType) {
            return (new[] { EntityType.BuilderUnit, EntityType.MeleeUnit, EntityType.RangedUnit }).Contains(entityType);
        }
    }
}