﻿using Aicup2020;
using Aicup2020.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace aicup2020
{
	public class Needs
	{
		public PlayerView PlayerView;
		public DebugInterface DebugInterface;
		public List<Need> CurrentNeeds = new List<Need>();

		private List<Entity> _emptyEntityList = new List<Entity>();
		public List<Entity> GetMyEntity(EntityType type)
		{
			return MySett.MyEntityCategory[type];
		}

		private List<BalanceRanges> _balanceRanges = null;
		public List<BalanceRanges> BalanceRanges
		{
			get
			{
				if (_balanceRanges != null) return _balanceRanges;

				if (MySett.IsR1)
				{
					//ROUND1  
					_balanceRanges = new List<BalanceRanges>
					{
						new BalanceRanges(-1, 0.1),
						new BalanceRanges(10, 0.1),
						new BalanceRanges(15, 0.1),
						new BalanceRanges(20, 0.1),
						new BalanceRanges(30, 0.1),
						new BalanceRanges(40, 0.15),
						new BalanceRanges(60, 0.15),
						new BalanceRanges(65, 0.3), 
						new BalanceRanges(80, 0.99),
						new BalanceRanges(100, 1.2),
						new BalanceRanges(500, 1.5),
					};
				}

				if (MySett.IsR2)
				{
					_balanceRanges = new List<BalanceRanges> 
					{ 
						new BalanceRanges(-1, 0.0),
						new BalanceRanges(10, 0.0),
						new BalanceRanges(15, 0.0),
						new BalanceRanges(20, 0.0),
						new BalanceRanges(30, 0.2),
						new BalanceRanges(40, 0.3),
						new BalanceRanges(50, 0.3),
						new BalanceRanges(60, 0.5), 
						new BalanceRanges(65, 0.6),
						new BalanceRanges(80, 0.9),
						new BalanceRanges(100, 1.2),
						new BalanceRanges(500, 1.5),
					};
				}

				if (MySett.IsFinal)
				{
					_balanceRanges = new List<BalanceRanges>
					{
						new BalanceRanges(-1, 0.0),
						new BalanceRanges(10, 0.0),
						new BalanceRanges(15, 0.0),
						new BalanceRanges(20, 0.0),
						new BalanceRanges(30, 0.1),
						new BalanceRanges(40, 0.1),
						new BalanceRanges(50, 0.15),
						new BalanceRanges(60, 0.2),
						new BalanceRanges(70, 0.3),
						new BalanceRanges(80, 0.3),
						new BalanceRanges(100, 1.2),
						new BalanceRanges(500, 1.5),
					};
				}
				return _balanceRanges;
			}
		}

		public List<Need> CalcNeeds() 
		{
			CurrentNeeds = new List<Need>();
			
			//Нужно ли строить первый дом рабочих
			CurrentNeeds.AddRange(FirstBuilderBase(100));
			//Первый домик для лучников
			if (MySett.IsFinal)
				CurrentNeeds.AddRange(AddFirstAttackerBase(200, 45, 0, 60)); 
			else
				CurrentNeeds.AddRange(AddFirstAttackerBase(200, 24, 20, 29));

			//Первый домик для мечников
			//CurrentNeeds.AddRange(AddFirstMeleeAttackerBase(250, 21)); 
			//Нужно ли нарожать атакующих если атака раньше началась, чемо построили минимум строителей 
			//CurrentNeeds.AddRange(AddDefendersAttackers(300, 3));
			//Починка
			CurrentNeeds.AddRange(Repaire(400));
			//Рабочих меньше минимума
			//CurrentNeeds.AddRange(BuildersMinimum(500, 5));

			//Резерв для защиты
			//CurrentNeeds.AddRange(AddDefenceReserve(550, 30));

			CurrentNeeds.AddRange(BuilderTurretR1(550, 50, 10));
			CurrentNeeds.AddRange(BuilderTurretR2(550, 55, 10));
			CurrentNeeds.AddRange(BuilderTurretFinals(550, 55, 10));

			//Нужно ли нарожать атакующих и каких
			//CurrentNeeds.AddRange(AddAttackers(270));
			//Баланс рабочих и строителей
			CurrentNeeds.AddRange(BuildersAttackersBalance(600));

			//Нужно ли построить для увеличения Population
			CurrentNeeds.AddRange(AddPopulation(700, CurrentNeeds));


			//По одному строителю за тик
			//CurrentNeeds.AddRange(BuildersAddOne(600, 20, 60));
			//Нужно ли построить Туррель
			//CurrentNeeds.AddRange(BuilderTurretFinals(800, 40, 25)); 


			//Доп. домик для лучников
			CurrentNeeds.AddRange(AddAttackerBase(900, 80, 80));

			//Нужно ли нарожать атакующих и каких
			//CurrentNeeds.AddRange(AddAttackers(900)); 

			

			

			return CurrentNeeds;
		}

		public List<Need> Repaire(int priority) 
		{
			var repairableTypes = PlayerView.EntityProperties[EntityType.BuilderUnit].Repair.Value.ValidTargets.Except(MySett.NotToRepareTypes).ToList();
			var res = new List<Need>();
			var toRepairs = MySett.GetMyEntities(repairableTypes)
				.Where(e => (!e.Active || e.Health < PlayerView.EntityProperties[e.EntityType].MaxHealth)).ToList();

			toRepairs.RemoveAll(r => r.EntityType == EntityType.House && r.Active);

			foreach (var b in toRepairs) 
			{
				var need = new Need
				{
					NeedType = NeedTypes.Repair,
					Count = 1,
					Priority = priority,
					Target = b,
					OneEntityResources = 0 //PlayerView.EntityProperties[b.EntityType].ResourcePerHealth * (PlayerView.EntityProperties[b.EntityType].MaxHealth - b.Health)
				};
				res.Add(need);
			}

			return res;
		}

		public List<Need> AddDefenceReserve(int priority, int minBuilders) 
		{
			var res = new List<Need>();
			var countMyBuilders = GetMyEntity(EntityType.BuilderUnit).Count;
			if (countMyBuilders < minBuilders) return res;

			var countMyRanged = GetMyEntity(EntityType.RangedUnit).Count;

			var need = new Need
			{
				NeedType = NeedTypes.Reserve,
				Count = 1,
				Priority = priority,
				OneEntityResources = 10 * PlayerView.EntityProperties[EntityType.RangedUnit].InitialCost + countMyRanged,
			};
			res.Add(need);
			return res;
		}

		public List<Need> AddDefendersAttackers(int priority, int minBuilders)
		{
			var res = new List<Need>(); 
			var countMyBuilders = GetMyEntity(EntityType.BuilderUnit).Count;
			if (countMyBuilders > minBuilders) return res;

			var countAgressor = MySett.GetEnemyEntities(MySett.RangedAndMeleeAttackerTypes).Count(e => (e.Position.X + e.Position.Y) < PlayerView.MapSize / 2);
				
			var countDefenders = GetMyEntity(EntityType.RangedUnit).Count + GetMyEntity(EntityType.MeleeUnit).Count;

			if (countAgressor < countDefenders*1.2) {
				return res;
			}


			var rangedBases = GetMyEntity(EntityType.RangedBase).Where(e => e.Active).OrderByDescending(e => e.Position.X + e.Position.Y).ThenBy(e => e.Id).ToList();
			var i = 0;
			foreach (var b in rangedBases)
			{
				var need = new Need
				{
					NeedType = NeedTypes.CreateRanged,
					Count = 1,
					Priority = priority + i,
					OneEntityResources = PlayerView.EntityProperties[EntityType.RangedUnit].InitialCost + GetMyEntity(EntityType.RangedUnit).Count,
					Target = b,
					EntityType = EntityType.RangedUnit
				};
				res.Add(need);
				i++;
			}

			//melles  
			if (rangedBases.Count == 0)
			{
				//var mellesCount = MyAllEntities.Count(e => e.EntityType == EntityType.MeleeUnit);
				//var rangedCount = MyAllEntities.Count(e => e.EntityType == EntityType.RangedUnit);
				if (true) //(rangedCount > 5 && rangedCount * 0.4 > mellesCount) || MyPlayer.Resource > 550)
				{
					rangedBases = GetMyEntity(EntityType.MeleeBase).Where(e => e.Active).OrderByDescending(e => e.Position.X + e.Position.Y).ThenBy(e => e.Id).ToList();
					i = 0;
					foreach (var b in rangedBases)
					{
						var need = new Need
						{
							NeedType = NeedTypes.CreateMelee,
							Count = 1,
							Priority = priority + i, //+ 50 + i,
							OneEntityResources = PlayerView.EntityProperties[EntityType.MeleeUnit].InitialCost + GetMyEntity(EntityType.MeleeUnit).Count,
							Target = b,
							EntityType = EntityType.MeleeUnit
						};
						res.Add(need);
						i++;
					}
				}
			}

			return res;
		}

		public List<Need> BuildersAttackersBalance(int priority)
		{
			var res = new List<Need>();
			var attackers = GetMyEntity(EntityType.RangedUnit).Count + GetMyEntity(EntityType.MeleeUnit).Count;
			var attackerBases = GetMyEntity(EntityType.RangedBase).Count(e => e.Active) + GetMyEntity(EntityType.MeleeBase).Count(e => e.Active);
			var builderBases = GetMyEntity(EntityType.BuilderBase).Count(e => e.Active);
			var builders = GetMyEntity(EntityType.BuilderUnit).Count;

			var buildAttackers = builderBases == 0;

			var buildBoth = attackerBases > 0 && builderBases > 0;

			var balance = BalanceRanges.FirstOrDefault(b => b.MinBuilders > builders);
			if (balance == null) return res;

			var balanceVal = builders == 0 ? 1000.0 : (double)attackers / (double)builders;
			var balanceCoeff = balance.Coeff;
			//Защитное повышение кол-ва атакующих
			if (MySett.Map.MapBlocks10.Any(b => b.Lvl <= 3 && b.EnemyAttackers.Count > 0))
			{
				balanceCoeff = balanceCoeff * 1.5;
			}

			if (attackerBases > 0
				&& balanceVal < balanceCoeff
				)
			{
				buildAttackers = true;
			}

			if (buildBoth && balanceCoeff - balanceVal > 0.1)
			{
				buildBoth = false;
			}
			var maxTicks = 450;

			if (attackerBases > 0 
				&& ((PlayerView.CurrentTick > maxTicks && builders > 40)
				|| (PlayerView.CurrentTick > 650)
				)
			) {
				buildAttackers = true;
				buildBoth = false;
			}
			
			if (buildAttackers)
			{
				res = AddAttackers(priority);
				if (buildBoth)
				{
					res.AddRange(BuildersAddOne(priority + 20, 0, 1000));
				}
			}
			else
			{
				res = BuildersAddOne(priority, 0, 1000);
				if (buildBoth) {
					res.AddRange(AddAttackers(priority + 20));
				}
			}

			return res;
		}

		public List<Need> AddAttackers(int priority)
		{
			var res = new List<Need>();
			var rangedBases = MySett.GetMyEntities(EntityType.RangedBase).Where(e => e.Active).OrderByDescending(e => e.Position.X + e.Position.Y).ThenBy(e => e.Id).ToList();
			var i = 0;
			foreach (var b in rangedBases)
			{
				var need = new Need
				{
					NeedType = NeedTypes.CreateRanged,
					Count = 1,
					Priority = priority + i,
					OneEntityResources = PlayerView.EntityProperties[EntityType.RangedUnit].InitialCost + GetMyEntity(EntityType.RangedUnit).Count,
					Target = b,
					EntityType = EntityType.RangedUnit
				};
				res.Add(need);
				i++;
			}


			//melles 
			var meleeBases = GetMyEntity(EntityType.MeleeBase).OrderByDescending(e => e.Position.X + e.Position.Y).ThenBy(e => e.Id).ToList();
			if (false && meleeBases.Count > 0 && GetMyEntity(EntityType.RangedUnit).Count - 0 > GetMyEntity(EntityType.MeleeUnit).Count)
			{
				var mellesCount = MySett.GetMyEntities(EntityType.MeleeUnit).Count;
				var rangedCount = MySett.GetMyEntities(EntityType.RangedUnit).Count;
				if (true) //(rangedCount > 5 && rangedCount * 0.4 > mellesCount) || MyPlayer.Resource > 550)
				{
					i = 0; 
					foreach (var b in meleeBases)
					{
						var build = rangedBases.Count == 0; // || (rangedCount > 5 && rangedCount * 0.4 > mellesCount); // || MyPlayer.Resource > 750);
						if (!build) 
						{
							//ищем ближайших к базе врагов. Если они ближе чем на 15 от центра и там его приемущество по атаке, то рожаем
							var nearestEnemies = MySett.GetEnemyEntities(MySett.RangedAndMeleeAttackerTypes).Where(e => 
									MyUtils.Dist(e.Position, b.Position) <= 15
								)
								.OrderBy(e => MyUtils.Dist(e.Position, b.Position)).ThenBy(e => e.Id).ToList();

							if (nearestEnemies.Count > 0)
							{
								build = true;
							}
						}

						if (build)
						{
							var need = new Need
							{
								NeedType = NeedTypes.CreateMelee,
								Count = 1,
								Priority = priority - 1 + i, //+ 50 + i,
								OneEntityResources = PlayerView.EntityProperties[EntityType.MeleeUnit].InitialCost + GetMyEntity(EntityType.MeleeUnit).Count,
								Target = b,
								EntityType = EntityType.MeleeUnit
							};
							res.Add(need);
							i++;
						}
					}
				}
			}

			return res;
		}

		public List<Need> BuildersAddOne(int priority, int min, int max)
		{
			var res = new List<Need>();
			var buildersCount = MySett.GetMyEntities(EntityType.BuilderUnit).Count;
			var attackersCount = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes).Count;
			if (buildersCount < min)
			{
				return res;
			}

			if (buildersCount > max)
			{
				return res;
			}



			var need = new Need
			{
				NeedType = NeedTypes.CreateBuilder,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[EntityType.BuilderUnit].InitialCost + GetMyEntity(EntityType.BuilderUnit).Count
			};
			res.Add(need);

			return res;
		}

		public List<Need> BuildersMinimum(int priority, int min)
		{
			var res = new List<Need>();
			var buildersCount = MySett.GetMyEntities(EntityType.BuilderUnit).Count;
			if (buildersCount > min)
			{
				return res;
			}

			var need = new Need
			{
				NeedType = NeedTypes.CreateBuilder,
				Count = min - buildersCount,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[EntityType.BuilderUnit].InitialCost + GetMyEntity(EntityType.BuilderUnit).Count
			};
			res.Add(need);

			return res;
		}

		public List<Need> BuilderTurretR1(int priority, int buildersMinimum, int myAcctackersMinimum)
		{
			var res = new List<Need>();
			if (!MySett.IsR1) return res;

			var buildersCount = MySett.GetMyEntities(EntityType.BuilderUnit).Count;
			if (buildersCount < buildersMinimum)
			{
				return res;
			}

			var myAcctackersCount = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes).Count;
			if (myAcctackersCount < myAcctackersMinimum)
			{
				return res;
			}

			var notActiveTurrets = MySett.GetMyEntities(EntityType.Turret).Count(t => !t.Active);
			if (notActiveTurrets >= 1) return res;

			var turrets = MySett.GetMyEntities(EntityType.Turret).Count;
			//var attackerBases = MyAllEntities.Count(e => e.EntityType == EntityType.RangedBase || e.EntityType == EntityType.MeleeBase);
			//var houses = MySett.GetMyEntities(EntityType.House).Count;

			//Хватает туретов
			if ((MySett.PlayerView.CurrentTick / 90.0) < turrets)
			{
				return res;
			}

			var buildType = EntityType.Turret;

			var need = new Need
			{
				NeedType = NeedTypes.BuildTurret,
				EntityType = buildType,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[buildType].InitialCost + GetMyEntity(buildType).Count
			};
			res.Add(need);

			return res;
		}

		public List<Need> BuilderTurretR2(int priority, int buildersMinimum, int myAcctackersMinimum)
		{
			var res = new List<Need>();
			if (!MySett.IsR2) return res;

			var buildersCount = MySett.GetMyEntities(EntityType.BuilderUnit).Count;
			if (buildersCount < buildersMinimum)
			{
				return res;
			}

			var myAcctackersCount = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes).Count;
			if (myAcctackersCount < myAcctackersMinimum)
			{
				return res;
			}

			var notActiveTurrets = MySett.GetMyEntities(EntityType.Turret).Count(t => !t.Active);
			if (notActiveTurrets >= 1) return res;

			var turrets = MySett.GetMyEntities(EntityType.Turret).Count;

			//var attackerBases = MyAllEntities.Count(e => e.EntityType == EntityType.RangedBase || e.EntityType == EntityType.MeleeBase);
			//var houses = MySett.GetMyEntities(EntityType.House).Count;

			//Хватает туретов
			if ((MySett.PlayerView.CurrentTick / 90.0) < turrets)
			{
				return res;
			}

			var buildType = EntityType.Turret;

			var need = new Need
			{
				NeedType = NeedTypes.BuildTurret,
				EntityType = buildType,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[buildType].InitialCost + GetMyEntity(buildType).Count
			};
			res.Add(need);

			return res;
		}

		public List<Need> BuilderTurretFinals(int priority, int buildersMinimum, int myAcctackersMinimum)
		{
			var res = new List<Need>();
			if (!MySett.IsFinal) return res;

			var buildersCount = MySett.GetMyEntities(EntityType.BuilderUnit).Count;
			if (buildersCount < buildersMinimum)
			{
				return res;
			}

			var myAcctackersCount = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes).Count;
			if (myAcctackersCount < myAcctackersMinimum)
			{
				return res;
			}

			var turrets = MySett.GetMyEntities(EntityType.Turret).Count;
			var notActiveTurrets = MySett.GetMyEntities(EntityType.Turret).Count(t=> !t.Active);
			if (notActiveTurrets >= 1) return res;
			//var attackerBases = MyAllEntities.Count(e => e.EntityType == EntityType.RangedBase || e.EntityType == EntityType.MeleeBase);
			//var houses = MySett.GetMyEntities(EntityType.House).Count;

			//Хватает туретов
			if (((MySett.PlayerView.CurrentTick - 200) / 80.0) < turrets)
			{
				return res;
			}

			var buildType = EntityType.Turret;

			var need = new Need
			{
				NeedType = NeedTypes.BuildTurret,
				EntityType = buildType,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[buildType].InitialCost + GetMyEntity(buildType).Count
			};
			res.Add(need);

			return res;
		}

		
		public List<Need> AddFirstMeleeAttackerBase(int priority, int buildersMinimum)
		{
			var res = new List<Need>();

			var buildersCount = GetMyEntity(EntityType.BuilderUnit).Count;
			//var attackersCount = GetMyEntity(EntityType.RangedUnit).Count + GetMyEntity(EntityType.MeleeUnit).Count;
			var meleeBases = GetMyEntity(EntityType.MeleeBase).Count;

			if (buildersCount < buildersMinimum || meleeBases > 0)
			{
				return res; 
			}

			var need = new Need
			{
				NeedType = NeedTypes.BuildMeleeBase,
				EntityType = EntityType.MeleeBase,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[EntityType.MeleeBase].InitialCost
			};
			res.Add(need);

			return res;
		}

		public List<Need> AddFirstAttackerBase(int priority, int buildersMinimum, int resourcesPercent, int buildersMaximum)
		{
			var res = new List<Need>();

			var buildersCount = GetMyEntity(EntityType.BuilderUnit).Count;
			//var attackersCount = GetMyEntity(EntityType.RangedUnit).Count + GetMyEntity(EntityType.MeleeUnit).Count;
			var rangedBases = GetMyEntity(EntityType.RangedBase).Count;

			if (buildersCount < buildersMinimum || rangedBases > 0)
			{
				return res;
			}

			if (buildersCount < buildersMaximum)
			{
				//Считаем сколько места занято ресурсами в квадрате 20/20
				var resCount = 0;
				var searchSize = 25;
				for (var x = 0; x <= searchSize; x++) 
				{
					for (var y = 0; y <= searchSize; y++)
					{
						if (MySett.Map.Items[x, y].Entity?.EntityType == EntityType.Resource)
							resCount++;
					} 
				}

				if (resCount < (searchSize * searchSize * ((double)resourcesPercent / 100.0)))
				{
					return res;
				}
			}

			var need = new Need
			{
				NeedType = NeedTypes.BuildRangedBase,
				EntityType = EntityType.RangedBase,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[EntityType.RangedBase].InitialCost
			};
			res.Add(need);

			return res;
		}

		public List<Need> AddAttackerBase(int priority, int buildersMinimum, int attackersMinimum)
		{
			var res = new List<Need>();

			var buildersCount = GetMyEntity(EntityType.BuilderUnit).Count;
			var attackersCount = GetMyEntity(EntityType.RangedUnit).Count + GetMyEntity(EntityType.MeleeUnit).Count;
			var rangedBases = GetMyEntity(EntityType.RangedBase).Count;

			if ((buildersCount < buildersMinimum || attackersCount < attackersMinimum) || rangedBases == 0)
			{
				return res;
			}



			//домики и лучники 1:10
			var houses = GetMyEntity(EntityType.House).Count;
			
			if ((rangedBases > 2 || houses < rangedBases * 15.0)) return res;

			var need = new Need
			{
				NeedType = NeedTypes.BuildRangedBase,
				EntityType = EntityType.RangedBase,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[EntityType.RangedBase].InitialCost + GetMyEntity(EntityType.RangedBase).Count
			};
			res.Add(need);

			return res;
		}

		public List<Need> AddPopulation(int priority, List<Need> allNeeds)
		{
			var res = new List<Need>();

			var buildersCount = GetMyEntity(EntityType.BuilderUnit).Count;

			if (GetMyEntity(EntityType.House).Count(e => !e.Active) > (buildersCount > 20 ? (buildersCount > 40 ? 1 : 0) : 0)
				//|| GetMyEntity(EntityType.RangedBase).Count(e => !e.Active) > 0
				//|| GetMyEntity(EntityType.MeleeBase).Count(e => !e.Active) > 0
				)
			{
				return res;
			}

			if (allNeeds.Any(n => n.NeedType == NeedTypes.BuildRangedBase
			|| n.NeedType == NeedTypes.BuildMeleeBase))
			{
				return res;
			}

			var popProvide = MySett.MyAllEntity.Where(e => e.Active).Sum(e=>PlayerView.EntityProperties[e.EntityType].PopulationProvide);
			var popUse = MySett.MyAllEntity.Sum(e => PlayerView.EntityProperties[e.EntityType].PopulationUse);

			//Хватает популяции 
			if (popProvide > popUse * (buildersCount > 50 ? 1.2 : 1.25) ) {
				return res;
			}

			var buildType = EntityType.House;

			var need = new Need
			{
				NeedType = NeedTypes.BuildHouse,
				EntityType = buildType,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[buildType].InitialCost + GetMyEntity(buildType).Count
			};
			res.Add(need);
			return res;
		}



		/// <summary>
		/// Строим производителя билдеров если его нет.
		/// </summary>
		/// <param name="playerView"></param>
		/// <param name="debugInterface"></param>
		/// <returns></returns>
		public List<Need> FirstBuilderBase(int priority) 
		{
			var res = new List<Need>();
			if (MySett.GetMyEntities(EntityType.BuilderBase).Count > 0) {
				return res;
			}

			var need = new Need {
				NeedType = NeedTypes.BuildBuilderBase,
				EntityType = EntityType.BuilderBase,
				Count = 1,
				Priority = priority,
				OneEntityResources = PlayerView.EntityProperties[EntityType.BuilderBase].InitialCost + GetMyEntity(EntityType.BuilderBase).Count
			};
			res.Add(need);

			return res;
		}


		public BalanceCalcResult CalcStandartBalance(PlayerView playerView) 
		{
			var res = new BalanceCalcResult();
			res.BaseBuilder = 1;
			res.BaseHouse = 0;
			res.BaseMelee = 1;
			res.BaseRanged = 1;
			res.Builder = 3;
			res.Melee = 0;
			res.Ranged = 3;
			res.Resources = 500;

			return res;
		}

		public class Need
		{
			public int Priority { get; set; }
			public NeedTypes NeedType { get; set; }

			public Vec2Int? Position { get; set; }

			public int Count { get; set; }

			public bool InProcess { get; set; }

			public List<int> ProcessingUnits { get; set; } = new List<int>();

			public int OneEntityResources { get; set; }

			public Entity? Target { get; set; }

			public EntityType EntityType { get; set; }
		}

		public enum NeedTypes
		{
			Repair,
			BuildHouse,
			BuildBuilderBase,
			BuildMeleeBase,
			BuildRangedBase,
			BuildTurret,
			BuildWall,
			CreateBuilder,
			CreateMelee,
			Reserve,
			CreateRanged,
			CollectResource
		}



		public class BalanceCalcResult
		{ 
			public int Resources { get; set; }
			public int Melee { get; set; }

			public int Ranged { get; set; }

			public int Builder { get; set; }

			public int BaseHouse { get; set; }

			public int BaseMelee { get; set; }

			public int BaseRanged { get; set; }

			public int BaseBuilder { get; set; }
		}
	}

	public class BalanceRanges {
		public BalanceRanges(int minBuilders, double coeff) {
			MinBuilders = minBuilders;
			Coeff = coeff;
		}
		public int MinBuilders;
		public double Coeff;
	}
}
