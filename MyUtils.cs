﻿using Aicup2020.Model;
using System;

namespace aicup2020
{
	public static class MyUtils
	{
		public static Entity EmptyEntity = new Entity();
		public static int Dist(int aX, int aY, int bX, int bY)
		{
			return Math.Abs(aX - bX) + Math.Abs(aY - bY);
		}

		public static int Dist(Vec2Int a, Vec2Int b)
		{
			return Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y);
		}

		public static int ToInt(this Vec2Int pos)
		{
			return pos.X * 100 + pos.Y;
		}

		public static Vec2Int TrimAttackerTarget(Entity attacker, Vec2Int target, int maxDist = 6)
		{
			var dist = Dist(attacker.Position, target);
			if (dist <= maxDist)
			{
				return target;
			}
			var coeff = (double)maxDist / (double)dist;
			var x = attacker.Position.X + (target.X - attacker.Position.X) * coeff;
			var y = attacker.Position.Y + (target.Y - attacker.Position.Y) * coeff;
			return new Vec2Int((int)x, (int)y);
		}
	}

	
}
