﻿using aicup2020;
using Aicup2020.Model;
using System.Collections.Generic;
using System.Linq;
using static aicup2020.Needs;

namespace Aicup2020
{
	public partial class MyStrategy 
	{

        public void DistribAttackersByMapBlocks(ref Action action)
        {
            var allAttackers = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes, true);

            if (allAttackers.Count == 0) return;

            List<MapBlock> blocks = null;
            //var maxBuildersPos = GetMyEntity(EntityType.BuilderUnit).Count == 0 ? 39 : GetMyEntity(EntityType.BuilderUnit).Max(e => System.Math.Max(e.Position.X, e.Position.Y));
            var maxLvl = MySett.GetMyEntities(EntityType.BuilderUnit).Count > 60 ? 4 : 4; // System.Math.Max(maxBuildersPos / 10 + 1, 3);

            blocks = MySett.Map.MapBlocks10.Where(b =>
                (
                    (b.Lvl <= maxLvl && b.EnemyAttackers.Count > 0)
                    || (b.MyBuildersCount > 0 && b.EnemyAttackers.Count > 0)
                    || (b.MyBuildingsCount > 0 && b.EnemyAttackers.Count > 0)
                )
            //&& !(b.Position.X == 30 && b.Position.Y == 30)
            ).ToList();

            if (blocks.Count == 0) return;

            if (!MySett.IsEnemyOnRight.HasValue)
            {
                if (blocks.Any(b => b.EnemyAttackers.Count > 2 && b.Position.X >= b.Position.Y))
                {
                    MySett.IsEnemyOnRight = true;
                }

                if (blocks.Any(b => b.EnemyAttackers.Count > 2 && b.Position.X < b.Position.Y))
                {
                    MySett.IsEnemyOnRight = false;
                }
            }

            /*
            for (var lvl = 1; lvl <= maxLvl; lvl++)
			{
                var lvlPlus = 0; // lvl < 3 ? 1 : 0;
                blocks = MySett.Map.MapBlocks10.Where(b => (b.Lvl == lvl || b.Lvl == lvl + lvlPlus) && b.EnemyAttackers.Count > 0).ToList();
                if (blocks.Any()) break;
			}  
            */


            var sumBlocksAttackers = blocks.Sum(b => b.EnemyAttackers.Count);
            var attackersCoeff = System.Math.Min(1, allAttackers.Count()*0.9 / sumBlocksAttackers);
            
            if (false && MySett.IsR1)
            {
                blocks = blocks.OrderByDescending(b => b.MyBuildersCount).OrderBy(b => b.Lvl).ThenByDescending(b => b.EnemyAttackers.Count).ToList();
            }
            else
            {
                blocks = blocks
                    .OrderBy(b => b.Lvl)
                    .ThenByDescending(b => b.EnemyAttackers.Count)
                    .ThenByDescending(b => b.MyBuildersCount)
                    .ThenByDescending(b => b.MyBuildingsCount)
                    .ToList();
            }

            var totalAttackers = allAttackers.Count();
            foreach (var block in blocks)
            {
                var coeff = ((double)block.EnemyAttackers.Count / (double)sumBlocksAttackers) * attackersCoeff;
                var myUnitsForBlockCount = System.Math.Min((int)((double)totalAttackers * coeff), (int)System.Math.Ceiling(block.EnemyAttackers.Count * 1.0));
                if (myUnitsForBlockCount == 0) myUnitsForBlockCount = 1; //continue;

                if (block.MyBuildersCount > 2)
                {
                    myUnitsForBlockCount = (int)System.Math.Ceiling(myUnitsForBlockCount * 1.3);
                }
                else if ((MySett.IsEnemyOnRight ?? false) && block.Position.X >= block.Position.Y)
                {
                    myUnitsForBlockCount = (int)System.Math.Ceiling(myUnitsForBlockCount * 1.1);
                }
                else if (!(MySett.IsEnemyOnRight ?? false) && block.Position.X < block.Position.Y)
                {
                    myUnitsForBlockCount = (int)System.Math.Ceiling(myUnitsForBlockCount * 1.1);
                }

                var myAttackers = allAttackers
                    .Where(e=>MyUtils.Dist(e.Position, block.Position) < PlayerView.MapSize * 1.7)
                    .OrderBy(e => MyUtils.Dist(e.Position.X, e.Position.Y, block.Position.X + block.Size / 2, block.Position.Y + block.Size / 2))
                    .Take(myUnitsForBlockCount).ToList();

                foreach (var attacker in myAttackers)
                {
                    MySett.MyEntityWithActionIds.Add(attacker.Id);
                    allAttackers.RemoveAll(a => a.Id == attacker.Id);
                    MoveAction? moveAction = null;
                    bool noAutoAttack = false;
                    bool needMove = true; 

                    moveAction = GetAttackDefenceAction(attacker);
                    if (moveAction != null)
                    {
                        noAutoAttack = true;
                        needMove = false;
                    }

                    if (needMove)
                    {
                        moveAction = new MoveAction
                        {
                            Target = new Vec2Int(block.Position.X + block.Size / 2, block.Position.Y + block.Size / 2),
                            BreakThrough = true,
                            FindClosestPosition = true
                        };
                    }


                    action.EntityActions[attacker.Id] = new EntityAction
                    {
                        MoveAction = moveAction,
                        AttackAction = new AttackAction
                        {
                            AutoAttack = noAutoAttack ? null : new AutoAttack
                            {
                                PathfindRange = 12, //PlayerView.MaxPathfindNodes,   
                                ValidTargets = System.Array.Empty<EntityType>() //Map.HasMyEntityTypeUnderAttack(attacker, EntityType.BuilderUnit) ? TypesToAttackNoBuilders :  new[] { EntityType.Resource }
                            }
                        }
                    };
                }
            }
        }

    }
}
