﻿using aicup2020;
using Aicup2020.Model;
using System.Collections.Generic;
using System.Linq;
using static aicup2020.Needs;

namespace Aicup2020
{
	public partial class MyStrategy
	{
		public void MoveSaboteursY(ref Action action) {
			//ищем куда идти
			var targetBuilders = new HashSet<int>();
			foreach (var entity in MySett.SaboteursY.Units.OrderByDescending(e=>e.Position.Y).ThenBy(e=>e.Id))
			{
				var helper = new BfsSaboteurHelper(targetBuilders, entity);
				var path = new Bfs().FindFirstNode(entity.Position, helper.IsValidPos, helper.IsTargetY);
				if (path == null)
				{
					path = new Bfs().FindFirstNode(entity.Position, helper.IsValidPosWithoutResources, helper.IsTargetY);
				}
				
				if (path == null && !PlayerView.FogOfWar)
				{
					path = new Bfs().FindFirstNode(entity.Position, helper.IsValidPos, helper.IsTargetFarFromHomeY);
				}
				
				if (path == null)
				{
					path = new Bfs().FindFirstNode(entity.Position, helper.IsValidPosWithoutResources, helper.IsTargetFarFromHomeY);
				}
				//Действие по пути 
				if (path != null)
				{
					MySett.MyEntityWithActionIds.Add(entity.Id);
					var firstStep = path.Path.First();
					if (MySett.Map.Items[firstStep.X, firstStep.Y].Entity == null)
					{
						MySett.Map.MoveItem(entity.Position, firstStep);
					}

					var move_action = new MoveAction
					{
						Target = new Vec2Int { X = firstStep.X, Y = firstStep.Y },
						FindClosestPosition = false,
						BreakThrough = false //entity.Position.X < PlayerView.MapSize * 0.8 || entity.Position.Y < PlayerView.MapSize * 0.8
					};

					action.EntityActions[entity.Id] = new EntityAction
					{
						MoveAction = move_action,
						BuildAction = null,
						RepairAction = null,
						AttackAction = new AttackAction
						{
							Target = null,
							AutoAttack = new AutoAttack
							{
								PathfindRange = 5,
								ValidTargets = System.Array.Empty<EntityType>() // new[] { EntityType.BuilderUnit, EntityType.BuilderBase, EntityType.MeleeBase, EntityType.RangedBase, EntityType.Wall, EntityType.House }
							}
						}
					};

					if (MySett.Map.Items[path.Target.X, path.Target.Y].Entity != null
						&& MySett.Map.Items[path.Target.X, path.Target.Y].Entity.Value.EntityType == EntityType.BuilderUnit
						&& MySett.Map.Items[path.Target.X, path.Target.Y].Entity.Value.PlayerId != MySett.MyPlayerId
						)
					{
						targetBuilders.Add(path.Target.ToInt());
					}

					
					//TODO: Толкать своих строителей
				}
			}
		}

		public void MoveSaboteursX(ref Action action)
		{
			//ищем куда идти
			var targetBuilders = new HashSet<int>();
			foreach (var entity in MySett.SaboteursX.Units.OrderByDescending(e => e.Position.X).ThenBy(e => e.Id))
			{
				var helper = new BfsSaboteurHelper(targetBuilders, entity);
				var path = new Bfs().FindFirstNode(entity.Position, helper.IsValidPos, helper.IsTargetX);
				if (path == null)
				{
					path = new Bfs().FindFirstNode(entity.Position, helper.IsValidPosWithoutResources, helper.IsTargetX);
				}
				
				if (path == null && !PlayerView.FogOfWar)
				{
					path = new Bfs().FindFirstNode(entity.Position, helper.IsValidPos, helper.IsTargetFarFromHomeX);
				}
				
				if (path == null)
				{
					path = new Bfs().FindFirstNode(entity.Position, helper.IsValidPosWithoutResources, helper.IsTargetFarFromHomeX);
				}

				//Действие по пути
				if (path != null)
				{
					MySett.MyEntityWithActionIds.Add(entity.Id);
					var firstStep = path.Path.First();
					if (MySett.Map.Items[firstStep.X, firstStep.Y].Entity == null)
					{
						MySett.Map.MoveItem(entity.Position, firstStep);
					}

					var move_action = new MoveAction
					{
						Target = new Vec2Int { X = firstStep.X, Y = firstStep.Y },
						FindClosestPosition = false,
						BreakThrough = false //entity.Position.X < PlayerView.MapSize * 0.8 || entity.Position.Y < PlayerView.MapSize * 0.8
					};

					action.EntityActions[entity.Id] = new EntityAction
					{
						MoveAction = move_action,
						BuildAction = null,
						RepairAction = null,
						AttackAction = new AttackAction
						{
							Target = null,
							AutoAttack = new AutoAttack
							{
								PathfindRange = 5,
								ValidTargets = System.Array.Empty<EntityType>() //new[] { EntityType.BuilderUnit, EntityType.BuilderBase, EntityType.MeleeBase, EntityType.RangedBase, EntityType.Wall, EntityType.House }
							}
						}
					};

					if (MySett.Map.Items[path.Target.X, path.Target.Y].Entity != null
						&& MySett.Map.Items[path.Target.X, path.Target.Y].Entity.Value.EntityType == EntityType.BuilderUnit
						&& MySett.Map.Items[path.Target.X, path.Target.Y].Entity.Value.PlayerId != MySett.MyPlayerId
						)
					{
						targetBuilders.Add(path.Target.ToInt());
					}


					//TODO: Толкать своих строителей
				}
			}
		}

		public void SetSaboteurs(ref Action action) {
			//Выделяем 10-20 % из общего числа. Включаем/Исключаем из группы  
			var allAttackersCount = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes, true).Count;
			var requiredCount = 0;
			var minTick = 0;
			if (MySett.IsR1)
				minTick = 350;

			var coeff = 0.2;
			if (MySett.IsFinal) coeff = 0.2;
			if (MySett.IsR1) coeff = 0.2;

			if (MySett.PlayerView.CurrentTick < minTick)
			{
				coeff = 0.0;
			}

			requiredCount = (int)System.Math.Floor(allAttackersCount * coeff);
			if (allAttackersCount >= 5 && (MySett.IsFinal) && requiredCount > 0)
			{
				requiredCount = 2;
			}

			var requiredCountX = (int)System.Math.Ceiling((double)requiredCount / 2.0);
			var requiredCountY = requiredCount - requiredCountX;

			//Актуализируем кто уже умер
			MySett.SaboteursX.Units = MySett.GetMyEntities(EntityType.RangedUnit).Where(e => MySett.SaboteursX.Units.Any(s => s.Id == e.Id)).ToList();
			MySett.SaboteursY.Units = MySett.GetMyEntities(EntityType.RangedUnit).Where(e => MySett.SaboteursY.Units.Any(s => s.Id == e.Id)).ToList();

			//Убираем тех кто уже попал под атаку
			MySett.SaboteursX.Units.RemoveAll(u => MySett.Map.Items[u.Position.X, u.Position.Y].AttackDamage > 0);
			MySett.SaboteursY.Units.RemoveAll(u => MySett.Map.Items[u.Position.X, u.Position.Y].AttackDamage > 0);

			//Убираем лишних
			if (MySett.SaboteursX.Units.Count > requiredCountX) {
				var toRemove = MySett.SaboteursX.Units.OrderBy(e => e.Position.X).Take(MySett.SaboteursX.Units.Count - requiredCountX);
				MySett.SaboteursX.Units.RemoveAll(e => toRemove.Any(r => r.Id == e.Id));
			}
			if (MySett.SaboteursY.Units.Count > requiredCountY)
			{
				var toRemove = MySett.SaboteursY.Units.OrderBy(e => e.Position.Y).Take(MySett.SaboteursY.Units.Count - requiredCountY);
				MySett.SaboteursY.Units.RemoveAll(e => toRemove.Any(r => r.Id == e.Id));
			}

			//Ставим как занятых
			foreach(var e in MySett.SaboteursX.Units)
			{
				MySett.MyEntityWithActionIds.Add(e.Id);
			}
			foreach (var e in MySett.SaboteursY.Units)
			{
				MySett.MyEntityWithActionIds.Add(e.Id);
			}

			//Добавляем недостающих
			if (MySett.SaboteursX.Units.Count < requiredCountX)
			{
				var toAdd = MySett.GetMyEntities(EntityType.RangedUnit, true)
					.Where(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackedBy.Count == 0
					&& MySett.Map.Items[e.Position.X, e.Position.Y].AttackedByNear.Count == 0
					&& e.Position.X >= e.Position.Y
					)
					.OrderByDescending(e => e.Position.X)
					.ThenBy(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackedByNear.Count > 0 ? 1 : 0)
					.Take(requiredCountX - MySett.SaboteursX.Units.Count)
					.ToList();

				MySett.SaboteursX.Units.AddRange(toAdd);
				toAdd.ForEach(e=> { MySett.MyEntityWithActionIds.Add(e.Id); });
				

				//Если не нашли в своей половине
				if (MySett.SaboteursX.Units.Count < requiredCountX)
				{
					toAdd = MySett.GetMyEntities(EntityType.RangedUnit, true)
						.Where(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackedBy.Count == 0
						&& MySett.Map.Items[e.Position.X, e.Position.Y].AttackedByNear.Count == 0
						//&& e.Position.X >= e.Position.Y
						)
						.OrderByDescending(e => e.Position.X)
						.ThenBy(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackedByNear.Count > 0 ? 1 : 0)
						.Take(requiredCountX - MySett.SaboteursX.Units.Count)
						.ToList();

					MySett.SaboteursX.Units.AddRange(toAdd);
					toAdd.ForEach(e => { MySett.MyEntityWithActionIds.Add(e.Id); });
				}
			}

			if (MySett.SaboteursY.Units.Count < requiredCountY)
			{
				var toAdd = MySett.GetMyEntities(EntityType.RangedUnit, true)
					.Where(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackedBy.Count == 0
					&& MySett.Map.Items[e.Position.X, e.Position.Y].AttackedByNear.Count == 0
					&& e.Position.X < e.Position.Y
					)
					.OrderByDescending(e => e.Position.Y)
					.ThenBy(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackedByNear.Count > 0 ? 1 : 0)
					.Take(requiredCountY - MySett.SaboteursY.Units.Count)
					.ToList();

				MySett.SaboteursY.Units.AddRange(toAdd);
				toAdd.ForEach(e => { MySett.MyEntityWithActionIds.Add(e.Id); });


				//Если не нашли в своей половине
				if (MySett.SaboteursY.Units.Count < requiredCountY)
				{
					toAdd = MySett.GetMyEntities(EntityType.RangedUnit, true)
						.Where(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackedBy.Count == 0
						&& MySett.Map.Items[e.Position.X, e.Position.Y].AttackedByNear.Count == 0
						//&& e.Position.X < e.Position.Y
						)
						.OrderByDescending(e => e.Position.Y)
						.ThenBy(e => MySett.Map.Items[e.Position.X, e.Position.Y].AttackedByNear.Count > 0 ? 1 : 0)
						.Take(requiredCountY - MySett.SaboteursY.Units.Count)
						.ToList();

					MySett.SaboteursY.Units.AddRange(toAdd);
					toAdd.ForEach(e => { MySett.MyEntityWithActionIds.Add(e.Id); });
				}
			}

			if (requiredCount > 0)
			{
				MoveSaboteursX(ref action);
				MoveSaboteursY(ref action);
			}
		}
	}
}
