﻿using aicup2020;
using Aicup2020.Model;
using System.Collections.Generic;
using System.Linq;
using static aicup2020.Needs;

namespace Aicup2020
{
	public partial class MyStrategy
	{

        /// <summary>
        /// Чиним
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool TryToRepairBfs(ref Action action, Entity target)
        {
            var allFreeBuildersCount = MySett.GetMyEntities(EntityType.BuilderUnit, true).Count;
            //Ищем ближайших рабочих (2-х/3-х)
            var repairsCount = (target.EntityType == EntityType.RangedBase
                || target.EntityType == EntityType.MeleeBase
                || target.EntityType == EntityType.BuilderBase) ? 9 : 4;

            if (MySett.GetMyEntities(EntityType.BuilderUnit).Count > 40)
            {
                repairsCount = (
                    target.EntityType == EntityType.RangedBase
                    || target.EntityType == EntityType.MeleeBase
                    || target.EntityType == EntityType.BuilderBase
                ) ? 17 : 5;
            }
            else if (MySett.GetMyEntities(EntityType.BuilderUnit).Count > 20)
            {
                repairsCount =
                    (target.EntityType == EntityType.RangedBase
                    || target.EntityType == EntityType.MeleeBase
                    || target.EntityType == EntityType.BuilderBase) ? 15 : 4; 
            }

            var maxCount = (int)System.Math.Max(3, MySett.GetMyEntities(EntityType.BuilderUnit, true).Count * 0.5);
            repairsCount = (int)System.Math.Min(maxCount, repairsCount);

            //Список всех клеток вокруг здания 
            var allAroundPoints = MySett.Map.GetAllPointsAroundBuilding(target);
            //Считаем сколько наших уже на месте
            var onPlaceBuilders = allAroundPoints.Select(p => MySett.Map.Items[p.X, p.Y]).Where(e=> e.Entity?.EntityType == EntityType.BuilderUnit 
                && e.Entity?.PlayerId == MySett.MyPlayerId).ToList();

            foreach (var b in onPlaceBuilders) 
            {
                MySett.MyEntityWithActionIds.Add(b.Entity.Value.Id);
                MySett.Map.MarkItemAsActionTarget(b.Entity.Value.Position);
                action.EntityActions[b.Entity.Value.Id] =
                    new EntityAction
                    {
                        MoveAction = null,
                        BuildAction = null,
                        RepairAction = new RepairAction
                        {
                            Target = target.Id,
                        },
                        AttackAction = null
                    };
            }
            if (onPlaceBuilders.Count >= repairsCount) {
                return true;
            } 

            //Получаем список незанятых клеток вокруг здания. Исключаем, то что под атакой
            var freePointsAround = MySett.Map.GetEmptyPointsAroundBuilding(target, new EntityType[] { EntityType.MeleeUnit, EntityType.RangedUnit });
            List<BfsResult> bfsResults = new List<BfsResult>();
            var i = 0;
            HashSet<int> pointsAroundHash = new HashSet<int>(allAroundPoints.Select(p=>p.ToInt()));
            HashSet<int> buildersHash = new HashSet<int>(onPlaceBuilders.Select(b => b.Entity.Value.Position.ToInt())); ;
            var findCount = repairsCount - onPlaceBuilders.Count;
            for(var n = 0; n < findCount; n++)
			{
                List<BfsResult> tmpBfsResults = new List<BfsResult>();
                foreach (var p in freePointsAround)
                {
                    var bfsHelper = new BfsRepairHelper(pointsAroundHash, buildersHash);
                    var bfsRes = new Bfs().FindFirstNode(p, bfsHelper.IsValidPos, bfsHelper.IsTarget, 20);
                    if (bfsRes == null) continue;
                    tmpBfsResults.Add(bfsRes);
                }

                //Берем лучший
                if (tmpBfsResults.Count == 0)
                {
                    break;
                }
                else
                {
                    var bfsRes = tmpBfsResults.OrderBy(p => p.PathLen).ThenBy(p => p.Target.ToInt()).First();
                    freePointsAround.RemoveAll(p => p.X == bfsRes.Start.X && p.Y == bfsRes.Start.Y);
                    bfsResults.Add(bfsRes);
                    buildersHash.Add(bfsRes.Target.ToInt());
                }
            }
            

            //Берем самые короткие пути делаем первые шаги по ним начиная от самого короткого пути
            //Если место для первого шага уже занято, то берем следующий путь
            if (bfsResults.Count == 0) return false;
            bfsResults = bfsResults.OrderBy(p => p.PathLen).ThenBy(p=>p.Target.ToInt()).ToList();
            i = 0;
            foreach (var bfsRes in bfsResults)
			{
                var builder = MySett.Map.Items[bfsRes.Target.X, bfsRes.Target.Y];
                var firstStep = bfsRes.Path.Count == 1 ? bfsRes.Start : bfsRes.Path[bfsRes.Path.Count - 2];
                if (MySett.Map.Items[firstStep.X, firstStep.Y].Entity != null) {
                    continue;
                }

                var move_action = new MoveAction
                {
                    Target = firstStep,
                    FindClosestPosition = false,
                    BreakThrough = false
                };

                action.EntityActions[builder.Entity.Value.Id] =
                    new EntityAction
                    {
                        MoveAction = move_action,
                        BuildAction = null,
                        RepairAction = null,
                        AttackAction = null
                    };

                MySett.MyEntityWithActionIds.Add(builder.Entity.Value.Id);
                MySett.Map.MoveItem(builder.Entity.Value.Position, firstStep);
                i++;
                if (i + onPlaceBuilders.Count >= repairsCount) 
                    break;
            }

            return true;
        }


        /// <summary>
        /// Чиним
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool TryToRepair(ref Action action, Entity target)
        {
            //Ищем ближайших рабочих (2-х/3-х)
            var repairsCount = (target.EntityType == EntityType.RangedBase
                || target.EntityType == EntityType.MeleeBase
                || target.EntityType == EntityType.BuilderBase) ? 7 : 3;

            if (MySett.GetMyEntities(EntityType.BuilderUnit).Count > 40)
            {
                repairsCount = (
                    target.EntityType == EntityType.RangedBase
                    || target.EntityType == EntityType.MeleeBase
                    || target.EntityType == EntityType.BuilderBase
                ) ? 12 : 4;
            }
            else if (MySett.GetMyEntities(EntityType.BuilderUnit).Count > 20)
            {
                repairsCount =
                    (target.EntityType == EntityType.RangedBase
                    || target.EntityType == EntityType.MeleeBase
                    || target.EntityType == EntityType.BuilderBase) ? 11 : 4;
            }

            var targetPos = new Vec2Int(target.Position.X + PlayerView.EntityProperties[target.EntityType].Size / 2, target.Position.Y + PlayerView.EntityProperties[target.EntityType].Size / 2);

            var builders = MySett.GetMyEntities(EntityType.BuilderUnit, true).OrderBy(b => MyUtils.Dist(b.Position, targetPos)).ThenBy(b => b.Id).Take(repairsCount).ToList();
            if (builders.Count == 0) return false;

            foreach (var b in builders)
            {
                var props = PlayerView.EntityProperties[b.EntityType];
                var move_action = new MoveAction
                {
                    Target = targetPos,
                    FindClosestPosition = true,
                    BreakThrough = true //true  
                };

                action.EntityActions[b.Id] =
                    new EntityAction
                    {
                        MoveAction = move_action,
                        BuildAction = null,
                        RepairAction = new RepairAction
                        {
                            Target = target.Id,
                        },
                        AttackAction = new AttackAction
                        {
                            Target = null,
                            AutoAttack = null /* new AutoAttack
                            {
                                PathfindRange = PlayerView.MaxPathfindNodes,//props.SightRange, 
                                ValidTargets = System.Array.Empty<EntityType>() // new[] { EntityType.Resource }
                            }*/
                        }
                    };

                MySett.MyEntityWithActionIds.Add(b.Id);
            }

            return true;
        }

    }
}
