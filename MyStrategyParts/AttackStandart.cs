﻿using aicup2020;
using Aicup2020.Model;
using System.Collections.Generic;
using System.Linq;
using static aicup2020.Needs;

namespace Aicup2020
{
	public partial class MyStrategy
	{
        public void CreateStandartAttackActions(ref Action action)
        {
            var allAttackers = MySett.GetMyEntities(MySett.RangedAndMeleeAttackerTypes, true);
            var myBuildersCount = MySett.GetMyEntities(EntityType.BuilderUnit, false).Count;
            var attackToHealth = new Dictionary<int, int>();

            if (allAttackers.Count == 0) return;

            var isEnemyOnRight = MySett.IsEnemyOnRight ?? false;

            if (!PlayerView.FogOfWar)
            {
                if (!MySett.IsEnemyOnRight.HasValue)
                {
                    isEnemyOnRight = MySett.GetEnemyEntities(MySett.BuildingTypes).Any(e =>
                        e.Position.X > e.Position.Y
                        && e.Position.Y < PlayerView.MapSize / 2
                    );
                }
            }
            else
            {
                if (!MySett.IsEnemyOnRight.HasValue)
                {
                    var myBase = MySett.GetMyEntities(EntityType.RangedBase).FirstOrDefault();
                    if (myBase.Id != 0)
                    {
                        isEnemyOnRight = myBase.Position.X < myBase.Position.Y;
                    }
                }
            }

            foreach (var entity in allAttackers)
            {
                var props = PlayerView.EntityProperties[entity.EntityType];
                bool needMove = true;
                bool noAutoAttack = false;
                bool shortAutoAttack = true;
                Entity target = new Entity();
                MoveAction? move_action = null;
                var trimTarget = false; // entity.Position.X > 45 || entity.Position.Y > 45; //PlayerView.FogOfWar && ; 
                /* 
                var toCroudPoint = MySett.Map.GetMyAttackerPointToCroud(entity);
                if (toCroudPoint != null)
                {
                    move_action = new MoveAction
                    {
                        Target = toCroudPoint.Value,
                        FindClosestPosition = true,
                        BreakThrough = true // (entity.Position.X + entity.Position.Y) > (PlayerView.MapSize / 2)
                    };
                    shortAutoAttack = true;
                    needMove = false; 
                } 
                */

                move_action = GetAttackDefenceAction(entity);
                if (move_action != null)
                {
                    noAutoAttack = true;
                    needMove = false;
                }



                var waitingBatch = false;
                if (needMove)
                {
                    var blocks = MySett.Map.MapBlocks10.Where(b =>
                        (
                            (b.Lvl <= 3 && b.EnemyAttackers.Count > 0)
                            //|| (b.MyBuildersCount > 0 && b.EnemyAttackers.Count > 0) 
                        )
                    //&& !(b.Position.X == 30 && b.Position.Y == 30) 
                    ).ToList();

                    if (false 
                        && MySett.IsR1
                        && blocks.Count == 0 
                        && (allAttackers.Count() < 20 || myBuildersCount < 40)
                        && FirtsWaitingBatch) 
                    {
                        //needMove = false;
                        waitingBatch = true;
                        AttackerCollectPoint = !AttackerCollectPoint;
                        var targetPos = (entity.Id % 2 == 0) ? new Vec2Int { X = PlayerView.MapSize / 6, Y = (int)(PlayerView.MapSize / 3) } : new Vec2Int { X = (int)(PlayerView.MapSize / 3), Y = PlayerView.MapSize / 6 };
                        if (trimTarget)
                        {
                            targetPos = MyUtils.TrimAttackerTarget(entity, targetPos);
                        }
                        move_action = new MoveAction
                        {
                            Target = targetPos,
                            FindClosestPosition = true,
                            BreakThrough = true //  (entity.Position.X + entity.Position.Y) > (PlayerView.MapSize / 2) 
                        };
                    }
                    else
                    {
                        //FirtsWaitingBatch = false;
                    }


                    if (needMove)
                    {
                        //TODO: Вычитать цель из тех на кого целиться 

                        //Защита от нападающих на центр
                        /*
                        if (target.Position.X == 0 && target.Position.Y == 0)
                        {
                            target = MySett.GetEnemyEntities(MySett.RangedAndMeleeAttackerTypes).Where(e=>
                                e.Position.X < PlayerView.MapSize * 0.6
                                && e.Position.Y < PlayerView.MapSize * 0.6
                                && (e.Position.Y + e.Position.X) < PlayerView.MapSize * 0.8
                                && MyUtils.Dist(e.Position, entity.Position) < PlayerView.MapSize * 1.1
                                )
                            .OrderBy(e => (MyUtils.Dist(e.Position, entity.Position)*2) + (e.Position.X + e.Position.Y)*3).ThenBy(e => e.Id)
                            .FirstOrDefault();
                        }
                        */

                        //Атака на ближайшего нападающего
                        if (!waitingBatch && target.Position.X == 0 && target.Position.Y == 0
                            //&& System.Math.Max(entity.Position.X, entity.Position.Y) < 45
                            )
                        {
                            target = MySett.GetEnemyEntities(MySett.RangedAndMeleeAttackerTypes)
                                .Where(e => MyUtils.Dist(e.Position, entity.Position) < 25 
                                && System.Math.Max(e.Position.X, e.Position.Y) < 55
                                )
                            .OrderBy(e => MyUtils.Dist(e.Position, entity.Position)).ThenBy(e => e.Id)
                            .FirstOrDefault();
                        }

                        //Атака на ближайшего строителя  
                        if (!waitingBatch && target.Position.X == 0 && target.Position.Y == 0)
                        {
                            target = MySett.GetEnemyEntities(EntityType.BuilderUnit)
                                .Where(e => MyUtils.Dist(e.Position, entity.Position) < 40)
                            .OrderBy(e => MyUtils.Dist(e.Position, entity.Position)).ThenBy(e => e.Id)
                            .FirstOrDefault();
                        }

                        //Атака на ближайшую постройку
                        if (!waitingBatch && target.Position.X == 0 && target.Position.Y == 0)
                        {
                            target = MySett.GetEnemyEntities(TypesToAttackFirst)
                                .Where(e => MyUtils.Dist(e.Position, entity.Position) < 40)
                            .OrderBy(e => MyUtils.Dist(e.Position, entity.Position)).ThenBy(e => e.Id)
                            .FirstOrDefault();
                        }

                        

                        /*
                        if (target.Position.X == 0 && target.Position.Y == 0)
                        {
                            target = PlayerView.Entities.Where(e => e.PlayerId != MyId && e.PlayerId != 0 && TypesToAttackFirst.Contains(e.EntityType))
                            .OrderBy(e => MyUtils.Dist(e.Position, entity.Position)).ThenBy(e=>e.Id)
                            .FirstOrDefault();
                        }

                        if (target.Position.X == 0 && target.Position.Y == 0)
                        {
                            target = PlayerView.Entities.Where(e => e.PlayerId != MyId && e.PlayerId != 0 && TypesToAttackSecond.Contains(e.EntityType))
                            .OrderBy(e => MyUtils.Dist(e.Position, entity.Position)).ThenBy(e=>e.Id)
                            .FirstOrDefault();
                        }

                        if (target.Position.X == 0 && target.Position.Y == 0)
                        {
                            target = PlayerView.Entities.Where(e => e.PlayerId != MyId && e.PlayerId != 0 && TypesToAttackThird.Contains(e.EntityType))
                            .OrderBy(e => MyUtils.Dist(e.Position, entity.Position)).ThenBy(e=>e.Id)
                            .FirstOrDefault();
                        }

                        if (target.Position.X == 0 && target.Position.Y == 0)
                        {
                            target = PlayerView.Entities.Where(e => e.PlayerId != MyId && e.PlayerId != 0 && e.EntityType != EntityType.Resource)
                            .OrderBy(e => MyUtils.Dist(e.Position, entity.Position)).ThenBy(e=>e.Id)
                            .FirstOrDefault();
                        }
                        */
                        if (target.Position.X > 0 || target.Position.Y > 0)
                        {
                            if (true //target.Position.X + target.Position.Y < PlayerView.MapSize * 0.5
                                && MySett.Map.IsValid(target.Position.X, target.Position.Y))
                            {
                                var targetPos = new Vec2Int { X = target.Position.X, Y = target.Position.Y };
                                if (trimTarget)
                                {
                                    targetPos = MyUtils.TrimAttackerTarget(entity, targetPos);
                                }
                                move_action = new MoveAction
                                {
                                    Target = targetPos,
                                    FindClosestPosition = true,
                                    BreakThrough = true//(entity.Position.X + entity.Position.Y) > (PlayerView.MapSize * 0.70)
                                };
                            }
                        }
                        else if (!waitingBatch)
                        {
                            //Никого не видно - идем в места скопления силы 
                            Vec2Int targetPos = MySett.GetPointToGoIfNoTarget(isEnemyOnRight);

                            if (trimTarget)
                            {
                                targetPos = MyUtils.TrimAttackerTarget(entity, targetPos);
                            }

                            move_action = new MoveAction
                            {
                                Target = targetPos,
                                FindClosestPosition = true,
                                BreakThrough = true//(entity.Position.X + entity.Position.Y) > (PlayerView.MapSize * 0.70)
                            };
                        }
                    }
                }


                AttackAction? attackAction = null;  // GetAttackAction(entity, attackToHealth);

                if (attackAction == null)
                {
                    var autoAttackRange = waitingBatch ? 15 : 10;//PlayerView.MaxPathfindNodes;

                    if (trimTarget && !waitingBatch)
                    {
                        var mapData = MySett.Map.Items[entity.Position.X, entity.Position.Y];
                        if (mapData.AttackedBy.Count == 0 && mapData.AttackedByNear.Count == 0)
                        {
                            autoAttackRange = 10;
                        }
                    }

                    if (shortAutoAttack && !waitingBatch)
                    {
                        autoAttackRange = PlayerView.EntityProperties[entity.EntityType].Attack?.AttackRange ?? 2;
                    }

                    attackAction = noAutoAttack ? null : new AttackAction
                    {
                        Target = null,
                        AutoAttack = new AutoAttack
                        {
                            PathfindRange = autoAttackRange, //(int)(move_action.HasValue? (MyUtils.Dist(move_action.Value.Target, entity.Position) * 4) : PlayerView.MaxPathfindNodes),//props.SightRange, //waitingBatch ? 5 : props.SightRange,
                            ValidTargets = System.Array.Empty<EntityType>() //Map.HasMyEntityTypeUnderAttack(entity, EntityType.BuilderUnit) ? TypesToAttackNoBuilders : System.Array.Empty<EntityType>() // new[] { EntityType.Resource }
                        }
                    };
                }

                action.EntityActions[entity.Id] =
                    new EntityAction
                    {
                        MoveAction = move_action,
                        BuildAction = null,
                        RepairAction = null,
                        AttackAction = attackAction
                    };
            }
        }
    }
}
