﻿using aicup2020;
using Aicup2020.Model;
using System.Collections.Generic;
using System.Linq;
using static aicup2020.Needs;

namespace Aicup2020
{
	public partial class MyStrategy
	{
        public void CreateStandartCollectResourcesActions(ref Action action)
        {
            var targets = new HashSet<int>();
            //var allEnimies = PlayerView.Entities.Where(e => e.PlayerId != MyId && e.PlayerId != 0 && AttackerTypes.Contains(e.EntityType)).ToList();
            var myBuilders = MySett.GetMyEntities(EntityType.BuilderUnit, true).OrderBy(e => e.Id).ToList();
            var props = PlayerView.EntityProperties[EntityType.BuilderUnit];
            foreach (var entity in myBuilders)
            {
                MySett.MyEntityWithActionIds.Add(entity.Id);
                var hasResourceAround = MySett.Map.HasEntityAround(entity.Position, EntityType.Resource);
                bool underAttack = false;
                bool needMove = true; //!MySett.Map.HasResourceAround(entity.Position); //true;// !PlayerView.Entities.Any(e => e.EntityType == EntityType.Resource && MyUtils.Dist(e.Position, entity.Position) <= (props.SightRange-1));
                MoveAction? move_action = null;
                BfsResult nearestResourcePath = null;
                bool calcNearestResourcePath = false;

                //Спасаемся от атаки (и своих тоже) 
                if (
                    needMove 
                    //&& !hasResourceAround //(на ходу мне и так путь мимо них построят)
                    && MySett.Map.IsBuilderPointOnWayAttackers(entity.Position.X, entity.Position.Y, true)
                    )
                {
                    if (!calcNearestResourcePath)
                    {
                        calcNearestResourcePath = true;
                        nearestResourcePath = FindNearestResourcePath(entity.Position);
                    }

                    move_action = new MoveAction
                    {
                        Target = MySett.Map.GetBuilderPointToSaveFromAttack(entity.Position, nearestResourcePath?.Target),
                        FindClosestPosition = true,
                        BreakThrough = false //entity.Position.X < PlayerView.MapSize * 0.8 || entity.Position.Y < PlayerView.MapSize * 0.8
                    };
                    needMove = false;
                    underAttack = true;
                }

                //Спасаемся от атаки своих 
                /*
                if (
                    needMove
                    && hasResourceAround
                    && MySett.Map.IsBuilderPointOnWayMyAttackers(entity.Position.X, entity.Position.Y, false)
                    )
                {
                    if (nearestResource.Id == 0)
                        nearestResource = FindNearestResource(EmptyStringHashSet, entity.Position, entity.Id);

                    move_action = new MoveAction
                    {
                        Target = MySett.Map.GetBuilderPointToSaveFromAttack(entity.Position, nearestResource.Position),
                        FindClosestPosition = true,
                        BreakThrough = false //entity.Position.X < PlayerView.MapSize * 0.8 || entity.Position.Y < PlayerView.MapSize * 0.8
                    };
                    needMove = false;
                    underAttack = true;
                }
                */

                var autoAttackDist = 0;
                if (needMove && !hasResourceAround)
                {
                    //Идем к ближайшему ресурсу
                    if (!calcNearestResourcePath)
                    {
                        calcNearestResourcePath = true;
                        nearestResourcePath = FindNearestResourcePath(entity.Position);
                    }

                    if ((nearestResourcePath != null))
                    {
                        var firstStep = nearestResourcePath.Path.First();
                        MySett.Map.MoveItem(entity.Position, firstStep);

                        move_action = new MoveAction
                        {
                            Target = new Vec2Int { X = firstStep.X, Y = firstStep.Y },
                            FindClosestPosition = true,
                            BreakThrough = true //entity.Position.X < PlayerView.MapSize * 0.8 || entity.Position.Y < PlayerView.MapSize * 0.8
                        };

                        autoAttackDist = 5;

                        targets.Add(nearestResourcePath.Target.ToInt());
                        MySett.Map.MarkItemAsActionTarget(nearestResourcePath.Target);
                    }
                    else
                    {
                        move_action = new MoveAction
                        {
                            Target = new Vec2Int(PlayerView.MapSize / 2, PlayerView.MapSize / 2),
                            /*
							Target = entity.Position.X < entity.Position.Y ? 
                                new Vec2Int { X = entity.Position.X, Y = System.Math.Min(entity.Position.Y + 10, PlayerView.MapSize-1) }
                                : new Vec2Int { X = System.Math.Min(entity.Position.X + 10, PlayerView.MapSize-1), Y = entity.Position.Y },
                            */
                            FindClosestPosition = true,
                            BreakThrough = true //entity.Position.X < PlayerView.MapSize * 0.8 || entity.Position.Y < PlayerView.MapSize * 0.8
                        };
                        underAttack = true;
                    }
                }

                action.EntityActions[entity.Id] =
                    new EntityAction
                    {
                        MoveAction = move_action,
                        BuildAction = null,
                        RepairAction = null,
                        AttackAction = new AttackAction
                        {
                            Target = null,
                            AutoAttack = underAttack ? null : new AutoAttack
                            {
                                PathfindRange = autoAttackDist != 0 ? autoAttackDist : PlayerView.MaxPathfindNodes,//props.SightRange, 
                                ValidTargets = underAttack ? System.Array.Empty<EntityType>() : new[] { EntityType.Resource, EntityType.BuilderUnit }
                            }
                        }
                    };
            }
        }

    }
}
