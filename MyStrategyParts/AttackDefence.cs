﻿using aicup2020;
using Aicup2020.Model;
using System.Collections.Generic;
using System.Linq;
using static aicup2020.Needs;

namespace Aicup2020
{
	public partial class MyStrategy
	{

        public MoveAction? GetAttackDefenceAction(Entity entity)
        {
            MoveAction? move_action = null;

            if (true //entity.EntityType != EntityType.MeleeUnit 
                    && MySett.Map.GetMyEntityAttackPowerBalance(entity.Position.X, entity.Position.Y) < 0
                    //&& MySett.Map.GetDamage(entity.Position.X, entity.Position.Y, true) == 0
                    )
            {
                var anyAttackMyBuilders = false;

                //Идти в размен (balance == 0) если хоть один из моих атакующих может достать до моих строителей
                if (false) //MySett.Map.GetAttackPowerBalance(entity.Position.X, entity.Position.Y) == 0)
                {
                    if (true)
                    {
                        foreach (var attacker in MySett.Map.Items[entity.Position.X, entity.Position.Y].AttackedBy)
                        {
                            if (MySett.Map.Items[attacker.Position.X, attacker.Position.Y].AttackToPeacefull
                                .Any(e => e.PlayerId == MySett.MyPlayerId && !MySett.AttackerTypes.Contains(e.EntityType))
                                )
                            {
                                anyAttackMyBuilders = true;
                                break;
                            }
                            if (MySett.Map.Items[attacker.Position.X, attacker.Position.Y].AttackToNearPeacefull
                                .Any(e => e.PlayerId == MySett.MyPlayerId && !MySett.AttackerTypes.Contains(e.EntityType))
                                )
                            {
                                anyAttackMyBuilders = true;
                                break;
                            }
                        }
                    }

                    if (!anyAttackMyBuilders)
                    {
                        foreach (var attacker in MySett.Map.Items[entity.Position.X, entity.Position.Y].AttackedByNear)
                        {
                            if (MySett.Map.Items[attacker.Position.X, attacker.Position.Y].AttackToPeacefull
                                .Any(e => e.PlayerId == MySett.MyPlayerId && !MySett.AttackerTypes.Contains(e.EntityType))
                                )
                            {
                                anyAttackMyBuilders = true;
                                break;
                            }
                            if (MySett.Map.Items[attacker.Position.X, attacker.Position.Y].AttackToNearPeacefull
                                .Any(e => e.PlayerId == MySett.MyPlayerId && !MySett.AttackerTypes.Contains(e.EntityType))
                                )
                            {
                                anyAttackMyBuilders = true;
                                break;
                            }
                        }
                    }
                }

                if (!anyAttackMyBuilders)
                {
                    var savePoint = MySett.Map.GetMyAttackerPointToSaveFromAttack(entity.Position);
                    if (savePoint != null)
                    {
                        move_action = new MoveAction
                        {
                            Target = savePoint.Value,
                            FindClosestPosition = false,
                            BreakThrough = false // (entity.Position.X + entity.Position.Y) > (PlayerView.MapSize / 2)
                        };
                        //shortAutoAttack = true; // MySett.Map.GetDamage(entity.Position.X, entity.Position.Y, false) == 0;
                        //needMove = false; 

                    }
                }
            }

            return move_action;
        }

    }
}
