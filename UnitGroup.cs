﻿using Aicup2020.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aicup2020
{
	public class UnitGroup
	{
		public UnitGroupTypes GroupType { get; set; }

		public List<Entity> Units { get; set; } = new List<Entity>();

		public int? TargetUnitId { get; set; }

		public Vec2Int? TargetPosition { get; set; }

		public int Priority { get; set; }
	}

	public enum UnitGroupTypes
	{
		SaboteurX,
		SaboteurY,
		/// <summary>
		/// Добывальщики ресурсов
		/// </summary>
		ResourceTakers,
		/// <summary>
		/// Строители зданий
		/// </summary>
		Builders,
		/// <summary>
		/// Захватчики
		/// </summary>
		Attackers,
		/// <summary>
		/// Охранники баз
		/// </summary>
		BaseDefenders,
		/// <summary>
		/// Новорожденные. Собираются в группу
		/// </summary>
		NewBorns
	}
}
