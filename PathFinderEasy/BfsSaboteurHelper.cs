﻿using Aicup2020.Model;
using aicup2020;
using System.Collections.Generic;
using System.Linq;

namespace Aicup2020
{
	public class BfsSaboteurHelper
	{
		private HashSet<int> TargetBuilders;
		private Entity Attacker;
		public BfsSaboteurHelper(HashSet<int> targetBuilders, Entity attacker) 
		{
			TargetBuilders = targetBuilders;
			Attacker = attacker;
		}

		public bool IsValidPosWithoutResources(Vec2Int pos)
		{
			var mapData = MySett.Map.Items[pos.X, pos.Y];
			//if (PointsAroundHash.Contains(pos.GetPosHash())) return false;
			return (mapData.Entity == null
				|| (
					mapData.Entity.Value.EntityType == EntityType.BuilderUnit
					&& mapData.Entity.Value.PlayerId == MySett.MyPlayerId
					)
				|| mapData.Entity.Value.EntityType == EntityType.Resource
				|| (
					!MySett.AttackerTypes.Contains(mapData.Entity.Value.EntityType)
					&& !(mapData.Entity.Value.EntityType == EntityType.Resource)
					&& mapData.Entity.Value.PlayerId != MySett.MyPlayerId
					&& !TargetBuilders.Contains(pos.ToInt())
					)
				)
				&& mapData.AttackDamage == 0 && mapData.AttackNear == 0;
		}

		public bool IsValidPos(Vec2Int pos) 
		{
			var mapData = MySett.Map.Items[pos.X, pos.Y];
			//if (PointsAroundHash.Contains(pos.GetPosHash())) return false; 
			return (mapData.Entity == null 
				|| (
					mapData.Entity.Value.EntityType == EntityType.BuilderUnit 
					&& mapData.Entity.Value.PlayerId == MySett.MyPlayerId
					)
				|| (
					!MySett.AttackerTypes.Contains(mapData.Entity.Value.EntityType)
					&& !(mapData.Entity.Value.EntityType == EntityType.Resource)
					&& mapData.Entity.Value.PlayerId != MySett.MyPlayerId
					&& !TargetBuilders.Contains(pos.ToInt())
					)
				)
				&& mapData.AttackDamage == 0 && mapData.AttackNear == 0;
		}

		public bool IsTargetX(Vec2Int pos)
		{
			if (TargetBuilders.Contains(pos.ToInt())) return false;

			var mapData = MySett.Map.Items[pos.X, pos.Y];
			return true //!mapData.IsActionTarget
				&& pos.X >= pos.Y
				&& mapData.Entity != null
				&& !MySett.AttackerTypes.Contains(mapData.Entity.Value.EntityType)
				&& !(mapData.Entity.Value.EntityType == EntityType.Resource)
				&& !TargetBuilders.Contains(pos.ToInt())
				&& mapData.Entity?.PlayerId != MySett.MyPlayerId;
		}

		public bool IsTargetY(Vec2Int pos) 
		{
			if (TargetBuilders.Contains(pos.ToInt())) return false;

			var mapData = MySett.Map.Items[pos.X, pos.Y];
			return true //!mapData.IsActionTarget
				&& pos.X < pos.Y
				&& mapData.Entity != null
				&& !MySett.AttackerTypes.Contains(mapData.Entity.Value.EntityType)
				&& !(mapData.Entity.Value.EntityType == EntityType.Resource)
				&& !TargetBuilders.Contains(pos.ToInt())
				&& mapData.Entity?.PlayerId != MySett.MyPlayerId;
		}

		public bool IsTargetFarFromHomeX(Vec2Int pos)
		{
			if (TargetBuilders.Contains(pos.ToInt())) return false;

			var mapData = MySett.Map.Items[pos.X, pos.Y];
			return true //!mapData.IsActionTarget
				&& pos.X >= pos.Y
				//&& Attacker.Position.X + 3 < pos.X// && Attacker.Position.Y >= pos.Y
				&& (Attacker.Position.X + 3 < pos.X
					|| (Attacker.Position.X > 70 && ((pos.X + pos.Y) > (Attacker.Position.X + Attacker.Position.Y)))
				)
				&& (mapData.Entity == null
				|| (
					mapData.Entity.Value.EntityType == EntityType.BuilderUnit
					&& mapData.Entity.Value.PlayerId == MySett.MyPlayerId
					)
			);
		} 

		public bool IsTargetFarFromHomeY(Vec2Int pos)
		{
			if (TargetBuilders.Contains(pos.ToInt())) return false;

			var mapData = MySett.Map.Items[pos.X, pos.Y];
			return true //!mapData.IsActionTarget 
				&& pos.X < pos.Y
				&& (Attacker.Position.Y + 3 < pos.Y 
					|| (Attacker.Position.Y > 70 && ((pos.X + pos.Y) > (Attacker.Position.X + Attacker.Position.Y)))
				)
				&& (mapData.Entity == null
				|| (
					mapData.Entity.Value.EntityType == EntityType.BuilderUnit
					&& mapData.Entity.Value.PlayerId == MySett.MyPlayerId
					)
			);
		}
	}
}
