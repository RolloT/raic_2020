﻿using aicup2020;
using Aicup2020.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aicup2020
{
	public class Bfs
	{
        public static bool FindResourceIsTarget(Vec2Int pos)
		{
            return MySett.Map.Items[pos.X, pos.Y].Entity?.EntityType == EntityType.Resource;
		}


        Func<Vec2Int, bool> IsValidFunc;
        Func<Vec2Int, bool> IsTargetFunc;

        public BfsResult FindFirstNode(
            Vec2Int start, 
            Func<Vec2Int, bool> isValidFunc, 
            Func<Vec2Int, bool> isTargetFunc, 
            int maxPathLen = 38) 
        {
            IsValidFunc = isValidFunc;
            IsTargetFunc = isTargetFunc;

            HashSet<int> itemCovered = new HashSet<int>();
            Queue<BfsNode> queue = new Queue<BfsNode>();


            queue.Enqueue(new BfsNode(start, null, 0));
            var nodesChecked = 0;
            BfsNode resBfsNode = new BfsNode(start, null, 0);

            while (queue.Count > 0)
            {
                var element = queue.Dequeue();
                if (IsTarget(element.Pos))
				{
                    resBfsNode = element;
                    break;
				}

                var posHash = element.Pos.ToInt();
                if (itemCovered.Contains(posHash))
                    continue;
                else
                    itemCovered.Add(posHash);

                //Проверка max длины пути
                if (maxPathLen == 0 || element.PathLen < maxPathLen)
                {
                    if (!EnqueueChilds(element, queue))
                        continue;
                }
                
                nodesChecked++;
            }

            if (resBfsNode.PathLen == 0) return null;

            var res = new List<Vec2Int>();
            var currNode = resBfsNode;
            while (currNode.PathLen != 0) {
                res.Add(currNode.Pos);
                currNode = currNode.PrevNode;
            }

            return new BfsResult { 
                Start = start,
                Target = resBfsNode.Pos,
                Path = res.Reverse<Vec2Int>().ToList(),
                PathLen = resBfsNode.PathLen
            };
        }

        public bool IsTarget(Vec2Int pos)
		{
            return IsTargetFunc(pos);
        }

        public bool EnqueueChilds(BfsNode posNode, Queue<BfsNode> queue) 
        {
            var res = false;
            var pos = posNode.Pos;
            var len = posNode.PathLen + 1;
            var p = new Vec2Int(pos.X, pos.Y + 1);
            if (IsValidChild(p)) { queue.Enqueue(new BfsNode(p, posNode, len)); res = true; }

            p = new Vec2Int(pos.X, pos.Y - 1);
            if (IsValidChild(p)) { queue.Enqueue(new BfsNode(p, posNode, len)); res = true; }

            p = new Vec2Int(pos.X + 1, pos.Y);
            if (IsValidChild(p)) { queue.Enqueue(new BfsNode(p, posNode, len)); res = true; }

            p = new Vec2Int(pos.X - 1, pos.Y);
            if (IsValidChild(p)) { queue.Enqueue(new BfsNode(p, posNode, len)); res = true; }

            return res;
        }

        private bool IsValidChild(Vec2Int pos) {
            return MySett.Map.IsValid(pos.X, pos.Y) 
                && IsValidFunc(pos);
        }

        
    }

    public class BfsNode {
        public BfsNode(Vec2Int pos, BfsNode prevNode, int pathLen) {
            PrevNode = prevNode;
            Pos = pos;
            PathLen = pathLen;
        }

        public Vec2Int Pos;
        public BfsNode PrevNode;
        public int PathLen;
    }

    public class BfsResult {
        public Vec2Int Start;
        public Vec2Int Target;
        public List<Vec2Int> Path;
        public int PathLen;
    }
}
