﻿using Aicup2020.Model;
using aicup2020;
using System.Collections.Generic;

namespace Aicup2020
{
	public class BfsRepairHelper
	{
		private HashSet<int> PointsAroundHash;
		private HashSet<int> BuildersHash;

		public BfsRepairHelper(HashSet<int> pointsAroundHash, HashSet<int> buildersHash) 
		{
			PointsAroundHash = pointsAroundHash;
			BuildersHash = buildersHash;
		}

		public bool IsValidPos(Vec2Int pos) 
		{
			var mapData = MySett.Map.Items[pos.X, pos.Y];
			//if (PointsAroundHash.Contains(pos.GetPosHash())) return false;
			return (mapData.Entity == null 
				|| (
					mapData.Entity.Value.EntityType == EntityType.BuilderUnit 
					&& mapData.Entity.Value.PlayerId == MySett.MyPlayerId
					&& !BuildersHash.Contains(pos.ToInt())
					)
				)
				&& mapData.AttackDamage == 0; // && mapData.AttackNear == 0);
		}

		public bool IsTarget(Vec2Int pos)
		{
			if (BuildersHash.Contains(pos.ToInt())) return false;
			if (PointsAroundHash.Contains(pos.ToInt())) return false;
			var mapData = MySett.Map.Items[pos.X, pos.Y];
			return !mapData.IsActionTarget
				&& mapData.Entity != null
				&& mapData.Entity?.EntityType == EntityType.BuilderUnit 
				&& mapData.Entity?.PlayerId == MySett.MyPlayerId;
		}

	}
}
