﻿using Aicup2020.Model;
using aicup2020;

namespace Aicup2020
{
	public class BfsFindResourceHelper
	{
		public BfsFindResourceHelper() {
		}

		public bool IsValidPos(Vec2Int pos) 
		{
			var mapData = MySett.Map.Items[pos.X, pos.Y];
			return (mapData.Entity == null && mapData.AttackDamage == 0 && mapData.AttackNear == 0);
		}

		public bool IsTarget(Vec2Int pos)
		{
			return !MySett.Map.Items[pos.X, pos.Y].IsActionTarget && MySett.Map.HasEntityAround(pos,EntityType.Resource);
		}

	}
}
