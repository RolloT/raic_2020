using aicup2020;
using Aicup2020.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;

namespace Aicup2020
{
	public class Path
	{
		public List<PathNode> PathNodes;
		public int PathLen
		{
			get
			{
				return PathNodes.Count;
			}
		}

		public int GetCacheKey()
		{
			return Start.X + Start.Y * 100 + End.X * 10000 + End.Y * 1000000;
		}

		public int LastUsedTick;
		public Vec2Int Start;
		public Vec2Int End;

		public Vec2Int GetPathTarget(ref Entity unit)
		{
			var tilePos = PathNodes.Count > 2 ? PathNodes[2].Position :
				(PathNodes.Count > 1 ? PathNodes[1].Position : PathNodes[0].Position);

			var unitY = (int)unit.Position.Y;
			if (unitY > tilePos.Y)
			{
				return new Vec2Int(tilePos.X, tilePos.Y);
			}
			if (unitY < tilePos.Y)
			{
				return new Vec2Int(tilePos.X, tilePos.Y);
			}

			return new Vec2Int(tilePos.X, tilePos.Y);
		}
	}
	public class PathNode
	{
		// ���������� ����� �� �����.
		public Vec2Int Position { get; set; }
		// ����� ���� �� ������ (G).
		public int PathLengthFromStart { get; set; }
		// �����, �� ������� ������ � ��� �����.
		public PathNode CameFrom { get; set; }
		// ��������� ���������� �� ���� (H).
		public int HeuristicEstimatePathLength { get; set; }
		// ��������� ������ ���������� �� ���� (F).
		public int EstimateFullPathLength
		{
			get
			{
				return this.PathLengthFromStart + this.HeuristicEstimatePathLength;
			}
		}

		public PathNode NextPathNode { get; set; }
	}

	public class PathFinderEasy
	{

		private static List<PathNode> GetPathForNode(PathNode pathNode)
		{
			var result = new List<PathNode>();
			var currentNode = pathNode;
			while (currentNode != null)
			{
				result.Add(currentNode);
				currentNode = currentNode.CameFrom;
			}
			result.Reverse();
			return result;
		}

		private static int GetDistanceBetweenNeighbours()
		{
			return 1;
		}

		private static int GetHeuristicPathLength(Vec2Int from, Vec2Int to)
		{
			return MyUtils.Dist(from, to);
		}

		public static PathNode GetNextPathNodeIfAccessible(Vec2Int nextPoint, PathNode currNode, Map field, Vec2Int goal)
		{
			// ���������, ��� �� ����� �� ������� �����.
			if (nextPoint.X < 0 || nextPoint.X >= field.Items.GetLength(0))
				return null;
			if (nextPoint.Y < 0 || nextPoint.Y >= field.Items.GetLength(1))
				return null;

			//�� ��������� �� �����
			if (currNode.Position.X != nextPoint.X && currNode.Position.Y != nextPoint.Y)
				return null;

			var nextTile = field.Items[nextPoint.X, nextPoint.Y];

			if (nextTile.Entity != null)
				return null;

			// ��������� ������ ��� ����� ��������.
			var nextPathNode = new PathNode()
			{
				Position = nextPoint,
				CameFrom = currNode,
				PathLengthFromStart = currNode.PathLengthFromStart + GetDistanceBetweenNeighbours(),
				HeuristicEstimatePathLength = GetHeuristicPathLength(nextPoint, goal),
			};

			return nextPathNode;
		}

		private static Collection<PathNode> GetNeighbours(PathNode pathNode, Vec2Int goal, Map field)
		{
			var result = new Collection<PathNode>();

			// ��������� ������� �������� �������� �� ������� ������.
			Vec2Int[] neighbourPoints = new Vec2Int[4];
			neighbourPoints[0] = new Vec2Int(pathNode.Position.X + 1, pathNode.Position.Y);
			neighbourPoints[1] = new Vec2Int(pathNode.Position.X - 1, pathNode.Position.Y);
			neighbourPoints[2] = new Vec2Int(pathNode.Position.X, pathNode.Position.Y + 1);
			neighbourPoints[3] = new Vec2Int(pathNode.Position.X, pathNode.Position.Y - 1);

			foreach (var point in neighbourPoints)
			{
				var neighbourNode = GetNextPathNodeIfAccessible(point, pathNode, field, goal);
				// ���������, ��� �� ������ ����� ������.
				if (neighbourNode == null)
					continue;

				result.Add(neighbourNode);
			}
			return result;
		}

		public static List<PathNode> FindPath(Map field, Entity unit, Vec2Int goal, bool first = true)
		{
			List<PathNode> res = null;

			// ��� 1.
			var closedSet = new Collection<PathNode>();
			var openSet = new Collection<PathNode>();
			var start = new Vec2Int(unit.Position.X, unit.Position.Y);
			// ��� 2.
			PathNode startNode = new PathNode()
			{
				Position = start,
				CameFrom = null,
				PathLengthFromStart = 0,
				HeuristicEstimatePathLength = GetHeuristicPathLength(start, goal),
			};
			var startCameFromNode = new PathNode
			{
				Position = new Vec2Int(unit.Position.X, unit.Position.Y)
			};
			startNode.CameFrom = startCameFromNode;
			closedSet.Add(startCameFromNode);

			if (start.X == goal.X && start.Y == goal.Y)
			{
				res = new List<PathNode>();
				res.Add(startNode.CameFrom);
				res.Add(startNode);
				res.Add(startNode);
				return res;
			}

			openSet.Add(startNode);
			while (openSet.Count > 0)
			{
				// ��� 3.
				var currentNode = openSet.OrderBy(node =>
				  node.EstimateFullPathLength).ThenBy(node => node.Position.Y).First();
				// ��� 4.
				if (currentNode.Position.X == goal.X && currentNode.Position.Y == goal.Y)
				{
					//TODO: ���� ������� ������, �� ������� ����� ������ �������� ���� ��� �� �� ���������� ����
					var path = GetPathForNode(currentNode);
					if (res == null || res.Count > path.Count)
						res = path;

					if (first || res.Count < 1.5 * (Math.Abs(start.X - goal.X) + Math.Abs(start.Y - goal.Y)))
					{
						return res;
					}

				}
				// ��� 5.
				openSet.Remove(currentNode);
				closedSet.Add(currentNode);

				// ��� 6.
				foreach (var neighbourNode in GetNeighbours(currentNode, goal, field))
				{
					// ��� 7.
					var closeNode = closedSet.FirstOrDefault(node => node.Position.X == neighbourNode.Position.X && node.Position.Y == neighbourNode.Position.Y);
					if (closeNode != null)
					{
						//TODO: ���� ������ � �� �� ����� � ����� ���� �� ������ ������ ��� ����� ����������� ������ ����� ����� ������, �� ���������� ����� ��������
						if (currentNode.PathLengthFromStart > 250
							|| !(closeNode.PathLengthFromStart > currentNode.PathLengthFromStart + 1)
						)
						{
							continue;
						}
						else
						{
							closedSet.Remove(closeNode);
						}
					}

					var openNode = openSet.FirstOrDefault(node =>
					  node.Position.X == neighbourNode.Position.X && node.Position.Y == neighbourNode.Position.Y);
					// ��� 8.
					if (openNode == null)
					{
						openSet.Add(neighbourNode);
					}
					else if (openNode.PathLengthFromStart > neighbourNode.PathLengthFromStart)
					{
						// ��� 9.
						openNode.CameFrom = currentNode;
						openNode.PathLengthFromStart = neighbourNode.PathLengthFromStart;
					}
				}
			}
			// ��� 10.
			return res;
		}
	}
}